/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.14.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include <qcustomplot.h>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout_6;
    QSplitter *splitter_2;
    QSplitter *splitter;
    QWidget *layoutWidget;
    QHBoxLayout *horizontalLayout_LCDs;
    QVBoxLayout *verticalLayout_people;
    QSpacerItem *verticalSpacer;
    QLabel *label;
    QSpacerItem *verticalSpacer_4;
    QGridLayout *gridLayout_people;
    QLabel *label_p3;
    QLabel *label_p0;
    QLabel *label_p4;
    QLabel *label_p1;
    QLabel *label_p5;
    QLabel *label_p2;
    QVBoxLayout *verticalLayout_radar;
    QLabel *label_LCDs_radar;
    QGridLayout *gridLayout_radar;
    QLCDNumber *lcdNumber_radar_HR_p0;
    QLabel *label_HR_radar;
    QLCDNumber *lcdNumber_radar_RR_p0;
    QLabel *label_RR_radar;
    QSpacerItem *verticalSpacer_radar;
    QVBoxLayout *verticalLayout_camera;
    QLabel *label_LCDs_camera;
    QGridLayout *gridLayout_camera;
    QLCDNumber *lcdNumber_camera_HR_p4;
    QLCDNumber *lcdNumber_camera_HR_p0;
    QLabel *label_RR_camera;
    QLabel *label_HR_camera;
    QLCDNumber *lcdNumber_camera_HR_p3;
    QLCDNumber *lcdNumber_camera_HR_p1;
    QLCDNumber *lcdNumber_camera_HR_p2;
    QLCDNumber *lcdNumber_camera_HR_p5;
    QLCDNumber *lcdNumber_camera_RR_p0;
    QLCDNumber *lcdNumber_camera_RR_p1;
    QLCDNumber *lcdNumber_camera_RR_p2;
    QLCDNumber *lcdNumber_camera_RR_p3;
    QLCDNumber *lcdNumber_camera_RR_p4;
    QLCDNumber *lcdNumber_camera_RR_p5;
    QVBoxLayout *verticalLayout_integrate;
    QLabel *label_LCDs_integrate;
    QGridLayout *gridLayout_integrate;
    QLCDNumber *lcdNumber_integrate_RR_p0;
    QLabel *label_RR_integrate;
    QLabel *label_HR_integrate;
    QLCDNumber *lcdNumber_integrate_HR_p0;
    QSpacerItem *verticalSpacer_integrate;
    QSpacerItem *horizontalSpacer_LCDs;
    QWidget *layoutWidget1;
    QHBoxLayout *horizontalLayout_graphs;
    QSpacerItem *verticalSpacer_graphs;
    QVBoxLayout *verticalLayout_5;
    QSpacerItem *verticalSpacer_7;
    QHBoxLayout *horizontalLayout_graphs2;
    QCustomPlot *HR_plot;
    QCustomPlot *RR_plot;
    QSpacerItem *horizontalSpacer_graphs;
    QWidget *layoutWidget2;
    QVBoxLayout *verticalLayout_7;
    QVBoxLayout *verticalLayout_2;
    QSpacerItem *verticalSpacer_3;
    QCheckBox *checkBox_xCorr;
    QCheckBox *checkBox_FFT;
    QCheckBox *checkBox_SaveData;
    QCheckBox *checkBox_displayPlots;
    QCheckBox *checkBox_AutoDetectPorts;
    QVBoxLayout *verticalLayout;
    QPushButton *pushButton_start;
    QPushButton *pushButton_pause;
    QPushButton *pushButton_stop;
    QPushButton *pushButton_settings;
    QPushButton *pushButton_Refresh;
    QLabel *label_user_UART_port;
    QLineEdit *lineEdit_UART_port;
    QLabel *label_user_data_port;
    QLineEdit *lineEdit_data_port;
    QSpacerItem *verticalSpacer_2;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_graph_settings;
    QGridLayout *gridLayout_graph_settings;
    QLabel *label_dropdownlist;
    QComboBox *graphpatient_dropdownlist;
    QSpacerItem *verticalSpacer_6;
    QLabel *label_legend;
    QGridLayout *gridLayout_2;
    QLabel *label_legend_camera;
    QLabel *label_legend_integrate;
    QLabel *label_legend_radar;
    QWidget *widget_radar;
    QWidget *widget_camera;
    QWidget *widget_integrate;
    QSpacerItem *verticalSpacer_5;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1485, 999);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        verticalLayout_6 = new QVBoxLayout(centralWidget);
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setContentsMargins(11, 11, 11, 11);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        splitter_2 = new QSplitter(centralWidget);
        splitter_2->setObjectName(QString::fromUtf8("splitter_2"));
        splitter_2->setOrientation(Qt::Horizontal);
        splitter = new QSplitter(splitter_2);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setOrientation(Qt::Vertical);
        layoutWidget = new QWidget(splitter);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        horizontalLayout_LCDs = new QHBoxLayout(layoutWidget);
        horizontalLayout_LCDs->setSpacing(6);
        horizontalLayout_LCDs->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_LCDs->setObjectName(QString::fromUtf8("horizontalLayout_LCDs"));
        horizontalLayout_LCDs->setContentsMargins(0, 0, 0, 0);
        verticalLayout_people = new QVBoxLayout();
        verticalLayout_people->setSpacing(0);
        verticalLayout_people->setObjectName(QString::fromUtf8("verticalLayout_people"));
        verticalLayout_people->setSizeConstraint(QLayout::SetMinimumSize);
        verticalSpacer = new QSpacerItem(20, 35, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_people->addItem(verticalSpacer);

        label = new QLabel(layoutWidget);
        label->setObjectName(QString::fromUtf8("label"));
        sizePolicy.setHeightForWidth(label->sizePolicy().hasHeightForWidth());
        label->setSizePolicy(sizePolicy);
        QFont font;
        font.setFamily(QString::fromUtf8("Arial"));
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        label->setFont(font);

        verticalLayout_people->addWidget(label);

        verticalSpacer_4 = new QSpacerItem(20, 5, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_people->addItem(verticalSpacer_4);

        gridLayout_people = new QGridLayout();
        gridLayout_people->setSpacing(6);
        gridLayout_people->setObjectName(QString::fromUtf8("gridLayout_people"));
        gridLayout_people->setSizeConstraint(QLayout::SetFixedSize);
        gridLayout_people->setVerticalSpacing(6);
        label_p3 = new QLabel(layoutWidget);
        label_p3->setObjectName(QString::fromUtf8("label_p3"));
        label_p3->setFont(font);
        label_p3->setAlignment(Qt::AlignCenter);

        gridLayout_people->addWidget(label_p3, 5, 0, 1, 1);

        label_p0 = new QLabel(layoutWidget);
        label_p0->setObjectName(QString::fromUtf8("label_p0"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(label_p0->sizePolicy().hasHeightForWidth());
        label_p0->setSizePolicy(sizePolicy1);
        label_p0->setFont(font);
        label_p0->setAlignment(Qt::AlignCenter);

        gridLayout_people->addWidget(label_p0, 2, 0, 1, 1);

        label_p4 = new QLabel(layoutWidget);
        label_p4->setObjectName(QString::fromUtf8("label_p4"));
        label_p4->setFont(font);
        label_p4->setAlignment(Qt::AlignCenter);

        gridLayout_people->addWidget(label_p4, 6, 0, 1, 1);

        label_p1 = new QLabel(layoutWidget);
        label_p1->setObjectName(QString::fromUtf8("label_p1"));
        label_p1->setFont(font);
        label_p1->setAlignment(Qt::AlignCenter);

        gridLayout_people->addWidget(label_p1, 3, 0, 1, 1);

        label_p5 = new QLabel(layoutWidget);
        label_p5->setObjectName(QString::fromUtf8("label_p5"));
        label_p5->setFont(font);
        label_p5->setAlignment(Qt::AlignCenter);

        gridLayout_people->addWidget(label_p5, 7, 0, 1, 1);

        label_p2 = new QLabel(layoutWidget);
        label_p2->setObjectName(QString::fromUtf8("label_p2"));
        label_p2->setFont(font);
        label_p2->setAlignment(Qt::AlignCenter);

        gridLayout_people->addWidget(label_p2, 4, 0, 1, 1);


        verticalLayout_people->addLayout(gridLayout_people);


        horizontalLayout_LCDs->addLayout(verticalLayout_people);

        verticalLayout_radar = new QVBoxLayout();
        verticalLayout_radar->setSpacing(6);
        verticalLayout_radar->setObjectName(QString::fromUtf8("verticalLayout_radar"));
        label_LCDs_radar = new QLabel(layoutWidget);
        label_LCDs_radar->setObjectName(QString::fromUtf8("label_LCDs_radar"));
        sizePolicy.setHeightForWidth(label_LCDs_radar->sizePolicy().hasHeightForWidth());
        label_LCDs_radar->setSizePolicy(sizePolicy);
        QFont font1;
        font1.setFamily(QString::fromUtf8("Arial"));
        font1.setPointSize(18);
        font1.setBold(true);
        font1.setWeight(75);
        label_LCDs_radar->setFont(font1);
        label_LCDs_radar->setAutoFillBackground(false);
        label_LCDs_radar->setAlignment(Qt::AlignBottom|Qt::AlignHCenter);

        verticalLayout_radar->addWidget(label_LCDs_radar);

        gridLayout_radar = new QGridLayout();
        gridLayout_radar->setSpacing(6);
        gridLayout_radar->setObjectName(QString::fromUtf8("gridLayout_radar"));
        lcdNumber_radar_HR_p0 = new QLCDNumber(layoutWidget);
        lcdNumber_radar_HR_p0->setObjectName(QString::fromUtf8("lcdNumber_radar_HR_p0"));
        QSizePolicy sizePolicy2(QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(lcdNumber_radar_HR_p0->sizePolicy().hasHeightForWidth());
        lcdNumber_radar_HR_p0->setSizePolicy(sizePolicy2);
        lcdNumber_radar_HR_p0->setMinimumSize(QSize(0, 0));

        gridLayout_radar->addWidget(lcdNumber_radar_HR_p0, 1, 0, 1, 1);

        label_HR_radar = new QLabel(layoutWidget);
        label_HR_radar->setObjectName(QString::fromUtf8("label_HR_radar"));
        sizePolicy.setHeightForWidth(label_HR_radar->sizePolicy().hasHeightForWidth());
        label_HR_radar->setSizePolicy(sizePolicy);
        label_HR_radar->setFont(font);
        label_HR_radar->setAlignment(Qt::AlignCenter);

        gridLayout_radar->addWidget(label_HR_radar, 0, 0, 1, 1);

        lcdNumber_radar_RR_p0 = new QLCDNumber(layoutWidget);
        lcdNumber_radar_RR_p0->setObjectName(QString::fromUtf8("lcdNumber_radar_RR_p0"));
        sizePolicy2.setHeightForWidth(lcdNumber_radar_RR_p0->sizePolicy().hasHeightForWidth());
        lcdNumber_radar_RR_p0->setSizePolicy(sizePolicy2);
        lcdNumber_radar_RR_p0->setMinimumSize(QSize(0, 0));

        gridLayout_radar->addWidget(lcdNumber_radar_RR_p0, 1, 1, 1, 1);

        label_RR_radar = new QLabel(layoutWidget);
        label_RR_radar->setObjectName(QString::fromUtf8("label_RR_radar"));
        sizePolicy.setHeightForWidth(label_RR_radar->sizePolicy().hasHeightForWidth());
        label_RR_radar->setSizePolicy(sizePolicy);
        label_RR_radar->setFont(font);
        label_RR_radar->setAlignment(Qt::AlignCenter);

        gridLayout_radar->addWidget(label_RR_radar, 0, 1, 1, 1);


        verticalLayout_radar->addLayout(gridLayout_radar);

        verticalSpacer_radar = new QSpacerItem(20, 202, QSizePolicy::Minimum, QSizePolicy::Preferred);

        verticalLayout_radar->addItem(verticalSpacer_radar);


        horizontalLayout_LCDs->addLayout(verticalLayout_radar);

        verticalLayout_camera = new QVBoxLayout();
        verticalLayout_camera->setSpacing(6);
        verticalLayout_camera->setObjectName(QString::fromUtf8("verticalLayout_camera"));
        label_LCDs_camera = new QLabel(layoutWidget);
        label_LCDs_camera->setObjectName(QString::fromUtf8("label_LCDs_camera"));
        sizePolicy.setHeightForWidth(label_LCDs_camera->sizePolicy().hasHeightForWidth());
        label_LCDs_camera->setSizePolicy(sizePolicy);
        label_LCDs_camera->setFont(font1);
        label_LCDs_camera->setAutoFillBackground(false);
        label_LCDs_camera->setAlignment(Qt::AlignBottom|Qt::AlignHCenter);

        verticalLayout_camera->addWidget(label_LCDs_camera);

        gridLayout_camera = new QGridLayout();
        gridLayout_camera->setSpacing(6);
        gridLayout_camera->setObjectName(QString::fromUtf8("gridLayout_camera"));
        lcdNumber_camera_HR_p4 = new QLCDNumber(layoutWidget);
        lcdNumber_camera_HR_p4->setObjectName(QString::fromUtf8("lcdNumber_camera_HR_p4"));
        sizePolicy2.setHeightForWidth(lcdNumber_camera_HR_p4->sizePolicy().hasHeightForWidth());
        lcdNumber_camera_HR_p4->setSizePolicy(sizePolicy2);

        gridLayout_camera->addWidget(lcdNumber_camera_HR_p4, 5, 0, 1, 1);

        lcdNumber_camera_HR_p0 = new QLCDNumber(layoutWidget);
        lcdNumber_camera_HR_p0->setObjectName(QString::fromUtf8("lcdNumber_camera_HR_p0"));
        sizePolicy2.setHeightForWidth(lcdNumber_camera_HR_p0->sizePolicy().hasHeightForWidth());
        lcdNumber_camera_HR_p0->setSizePolicy(sizePolicy2);

        gridLayout_camera->addWidget(lcdNumber_camera_HR_p0, 1, 0, 1, 1);

        label_RR_camera = new QLabel(layoutWidget);
        label_RR_camera->setObjectName(QString::fromUtf8("label_RR_camera"));
        sizePolicy.setHeightForWidth(label_RR_camera->sizePolicy().hasHeightForWidth());
        label_RR_camera->setSizePolicy(sizePolicy);
        label_RR_camera->setFont(font);
        label_RR_camera->setAlignment(Qt::AlignCenter);

        gridLayout_camera->addWidget(label_RR_camera, 0, 1, 1, 1);

        label_HR_camera = new QLabel(layoutWidget);
        label_HR_camera->setObjectName(QString::fromUtf8("label_HR_camera"));
        sizePolicy.setHeightForWidth(label_HR_camera->sizePolicy().hasHeightForWidth());
        label_HR_camera->setSizePolicy(sizePolicy);
        label_HR_camera->setFont(font);
        label_HR_camera->setAlignment(Qt::AlignCenter);

        gridLayout_camera->addWidget(label_HR_camera, 0, 0, 1, 1);

        lcdNumber_camera_HR_p3 = new QLCDNumber(layoutWidget);
        lcdNumber_camera_HR_p3->setObjectName(QString::fromUtf8("lcdNumber_camera_HR_p3"));
        sizePolicy2.setHeightForWidth(lcdNumber_camera_HR_p3->sizePolicy().hasHeightForWidth());
        lcdNumber_camera_HR_p3->setSizePolicy(sizePolicy2);

        gridLayout_camera->addWidget(lcdNumber_camera_HR_p3, 4, 0, 1, 1);

        lcdNumber_camera_HR_p1 = new QLCDNumber(layoutWidget);
        lcdNumber_camera_HR_p1->setObjectName(QString::fromUtf8("lcdNumber_camera_HR_p1"));
        lcdNumber_camera_HR_p1->setEnabled(true);
        sizePolicy2.setHeightForWidth(lcdNumber_camera_HR_p1->sizePolicy().hasHeightForWidth());
        lcdNumber_camera_HR_p1->setSizePolicy(sizePolicy2);

        gridLayout_camera->addWidget(lcdNumber_camera_HR_p1, 2, 0, 1, 1);

        lcdNumber_camera_HR_p2 = new QLCDNumber(layoutWidget);
        lcdNumber_camera_HR_p2->setObjectName(QString::fromUtf8("lcdNumber_camera_HR_p2"));
        sizePolicy2.setHeightForWidth(lcdNumber_camera_HR_p2->sizePolicy().hasHeightForWidth());
        lcdNumber_camera_HR_p2->setSizePolicy(sizePolicy2);

        gridLayout_camera->addWidget(lcdNumber_camera_HR_p2, 3, 0, 1, 1);

        lcdNumber_camera_HR_p5 = new QLCDNumber(layoutWidget);
        lcdNumber_camera_HR_p5->setObjectName(QString::fromUtf8("lcdNumber_camera_HR_p5"));
        sizePolicy2.setHeightForWidth(lcdNumber_camera_HR_p5->sizePolicy().hasHeightForWidth());
        lcdNumber_camera_HR_p5->setSizePolicy(sizePolicy2);

        gridLayout_camera->addWidget(lcdNumber_camera_HR_p5, 6, 0, 1, 1);

        lcdNumber_camera_RR_p0 = new QLCDNumber(layoutWidget);
        lcdNumber_camera_RR_p0->setObjectName(QString::fromUtf8("lcdNumber_camera_RR_p0"));
        sizePolicy2.setHeightForWidth(lcdNumber_camera_RR_p0->sizePolicy().hasHeightForWidth());
        lcdNumber_camera_RR_p0->setSizePolicy(sizePolicy2);

        gridLayout_camera->addWidget(lcdNumber_camera_RR_p0, 1, 1, 1, 1);

        lcdNumber_camera_RR_p1 = new QLCDNumber(layoutWidget);
        lcdNumber_camera_RR_p1->setObjectName(QString::fromUtf8("lcdNumber_camera_RR_p1"));
        sizePolicy2.setHeightForWidth(lcdNumber_camera_RR_p1->sizePolicy().hasHeightForWidth());
        lcdNumber_camera_RR_p1->setSizePolicy(sizePolicy2);

        gridLayout_camera->addWidget(lcdNumber_camera_RR_p1, 2, 1, 1, 1);

        lcdNumber_camera_RR_p2 = new QLCDNumber(layoutWidget);
        lcdNumber_camera_RR_p2->setObjectName(QString::fromUtf8("lcdNumber_camera_RR_p2"));
        sizePolicy2.setHeightForWidth(lcdNumber_camera_RR_p2->sizePolicy().hasHeightForWidth());
        lcdNumber_camera_RR_p2->setSizePolicy(sizePolicy2);

        gridLayout_camera->addWidget(lcdNumber_camera_RR_p2, 3, 1, 1, 1);

        lcdNumber_camera_RR_p3 = new QLCDNumber(layoutWidget);
        lcdNumber_camera_RR_p3->setObjectName(QString::fromUtf8("lcdNumber_camera_RR_p3"));
        sizePolicy2.setHeightForWidth(lcdNumber_camera_RR_p3->sizePolicy().hasHeightForWidth());
        lcdNumber_camera_RR_p3->setSizePolicy(sizePolicy2);

        gridLayout_camera->addWidget(lcdNumber_camera_RR_p3, 4, 1, 1, 1);

        lcdNumber_camera_RR_p4 = new QLCDNumber(layoutWidget);
        lcdNumber_camera_RR_p4->setObjectName(QString::fromUtf8("lcdNumber_camera_RR_p4"));
        sizePolicy2.setHeightForWidth(lcdNumber_camera_RR_p4->sizePolicy().hasHeightForWidth());
        lcdNumber_camera_RR_p4->setSizePolicy(sizePolicy2);

        gridLayout_camera->addWidget(lcdNumber_camera_RR_p4, 5, 1, 1, 1);

        lcdNumber_camera_RR_p5 = new QLCDNumber(layoutWidget);
        lcdNumber_camera_RR_p5->setObjectName(QString::fromUtf8("lcdNumber_camera_RR_p5"));
        sizePolicy2.setHeightForWidth(lcdNumber_camera_RR_p5->sizePolicy().hasHeightForWidth());
        lcdNumber_camera_RR_p5->setSizePolicy(sizePolicy2);

        gridLayout_camera->addWidget(lcdNumber_camera_RR_p5, 6, 1, 1, 1);


        verticalLayout_camera->addLayout(gridLayout_camera);


        horizontalLayout_LCDs->addLayout(verticalLayout_camera);

        verticalLayout_integrate = new QVBoxLayout();
        verticalLayout_integrate->setSpacing(6);
        verticalLayout_integrate->setObjectName(QString::fromUtf8("verticalLayout_integrate"));
        label_LCDs_integrate = new QLabel(layoutWidget);
        label_LCDs_integrate->setObjectName(QString::fromUtf8("label_LCDs_integrate"));
        sizePolicy.setHeightForWidth(label_LCDs_integrate->sizePolicy().hasHeightForWidth());
        label_LCDs_integrate->setSizePolicy(sizePolicy);
        label_LCDs_integrate->setFont(font1);
        label_LCDs_integrate->setAlignment(Qt::AlignBottom|Qt::AlignHCenter);

        verticalLayout_integrate->addWidget(label_LCDs_integrate);

        gridLayout_integrate = new QGridLayout();
        gridLayout_integrate->setSpacing(6);
        gridLayout_integrate->setObjectName(QString::fromUtf8("gridLayout_integrate"));
        lcdNumber_integrate_RR_p0 = new QLCDNumber(layoutWidget);
        lcdNumber_integrate_RR_p0->setObjectName(QString::fromUtf8("lcdNumber_integrate_RR_p0"));
        sizePolicy2.setHeightForWidth(lcdNumber_integrate_RR_p0->sizePolicy().hasHeightForWidth());
        lcdNumber_integrate_RR_p0->setSizePolicy(sizePolicy2);

        gridLayout_integrate->addWidget(lcdNumber_integrate_RR_p0, 1, 1, 1, 1);

        label_RR_integrate = new QLabel(layoutWidget);
        label_RR_integrate->setObjectName(QString::fromUtf8("label_RR_integrate"));
        sizePolicy.setHeightForWidth(label_RR_integrate->sizePolicy().hasHeightForWidth());
        label_RR_integrate->setSizePolicy(sizePolicy);
        label_RR_integrate->setFont(font);
        label_RR_integrate->setAlignment(Qt::AlignCenter);

        gridLayout_integrate->addWidget(label_RR_integrate, 0, 1, 1, 1);

        label_HR_integrate = new QLabel(layoutWidget);
        label_HR_integrate->setObjectName(QString::fromUtf8("label_HR_integrate"));
        sizePolicy.setHeightForWidth(label_HR_integrate->sizePolicy().hasHeightForWidth());
        label_HR_integrate->setSizePolicy(sizePolicy);
        label_HR_integrate->setFont(font);
        label_HR_integrate->setAlignment(Qt::AlignCenter);

        gridLayout_integrate->addWidget(label_HR_integrate, 0, 0, 1, 1);

        lcdNumber_integrate_HR_p0 = new QLCDNumber(layoutWidget);
        lcdNumber_integrate_HR_p0->setObjectName(QString::fromUtf8("lcdNumber_integrate_HR_p0"));
        sizePolicy2.setHeightForWidth(lcdNumber_integrate_HR_p0->sizePolicy().hasHeightForWidth());
        lcdNumber_integrate_HR_p0->setSizePolicy(sizePolicy2);

        gridLayout_integrate->addWidget(lcdNumber_integrate_HR_p0, 1, 0, 1, 1);


        verticalLayout_integrate->addLayout(gridLayout_integrate);

        verticalSpacer_integrate = new QSpacerItem(20, 202, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_integrate->addItem(verticalSpacer_integrate);


        horizontalLayout_LCDs->addLayout(verticalLayout_integrate);

        horizontalSpacer_LCDs = new QSpacerItem(15, 20, QSizePolicy::Preferred, QSizePolicy::Minimum);

        horizontalLayout_LCDs->addItem(horizontalSpacer_LCDs);

        splitter->addWidget(layoutWidget);
        layoutWidget1 = new QWidget(splitter);
        layoutWidget1->setObjectName(QString::fromUtf8("layoutWidget1"));
        horizontalLayout_graphs = new QHBoxLayout(layoutWidget1);
        horizontalLayout_graphs->setSpacing(6);
        horizontalLayout_graphs->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_graphs->setObjectName(QString::fromUtf8("horizontalLayout_graphs"));
        horizontalLayout_graphs->setContentsMargins(0, 0, 0, 0);
        verticalSpacer_graphs = new QSpacerItem(20, 380, QSizePolicy::Minimum, QSizePolicy::Fixed);

        horizontalLayout_graphs->addItem(verticalSpacer_graphs);

        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        verticalSpacer_7 = new QSpacerItem(20, 15, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_5->addItem(verticalSpacer_7);

        horizontalLayout_graphs2 = new QHBoxLayout();
        horizontalLayout_graphs2->setSpacing(6);
        horizontalLayout_graphs2->setObjectName(QString::fromUtf8("horizontalLayout_graphs2"));
        HR_plot = new QCustomPlot(layoutWidget1);
        HR_plot->setObjectName(QString::fromUtf8("HR_plot"));
        QSizePolicy sizePolicy3(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(HR_plot->sizePolicy().hasHeightForWidth());
        HR_plot->setSizePolicy(sizePolicy3);
        HR_plot->setMinimumSize(QSize(471, 250));

        horizontalLayout_graphs2->addWidget(HR_plot);

        RR_plot = new QCustomPlot(layoutWidget1);
        RR_plot->setObjectName(QString::fromUtf8("RR_plot"));
        sizePolicy3.setHeightForWidth(RR_plot->sizePolicy().hasHeightForWidth());
        RR_plot->setSizePolicy(sizePolicy3);
        RR_plot->setMinimumSize(QSize(471, 250));

        horizontalLayout_graphs2->addWidget(RR_plot);


        verticalLayout_5->addLayout(horizontalLayout_graphs2);


        horizontalLayout_graphs->addLayout(verticalLayout_5);

        horizontalSpacer_graphs = new QSpacerItem(15, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_graphs->addItem(horizontalSpacer_graphs);

        splitter->addWidget(layoutWidget1);
        splitter_2->addWidget(splitter);
        layoutWidget2 = new QWidget(splitter_2);
        layoutWidget2->setObjectName(QString::fromUtf8("layoutWidget2"));
        verticalLayout_7 = new QVBoxLayout(layoutWidget2);
        verticalLayout_7->setSpacing(6);
        verticalLayout_7->setContentsMargins(11, 11, 11, 11);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        verticalLayout_7->setSizeConstraint(QLayout::SetDefaultConstraint);
        verticalLayout_7->setContentsMargins(0, 0, 0, 0);
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalSpacer_3 = new QSpacerItem(20, 60, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_2->addItem(verticalSpacer_3);

        checkBox_xCorr = new QCheckBox(layoutWidget2);
        checkBox_xCorr->setObjectName(QString::fromUtf8("checkBox_xCorr"));
        QFont font2;
        font2.setPointSize(12);
        checkBox_xCorr->setFont(font2);
        checkBox_xCorr->setChecked(false);

        verticalLayout_2->addWidget(checkBox_xCorr);

        checkBox_FFT = new QCheckBox(layoutWidget2);
        checkBox_FFT->setObjectName(QString::fromUtf8("checkBox_FFT"));
        checkBox_FFT->setFont(font2);

        verticalLayout_2->addWidget(checkBox_FFT);

        checkBox_SaveData = new QCheckBox(layoutWidget2);
        checkBox_SaveData->setObjectName(QString::fromUtf8("checkBox_SaveData"));
        checkBox_SaveData->setFont(font2);

        verticalLayout_2->addWidget(checkBox_SaveData);

        checkBox_displayPlots = new QCheckBox(layoutWidget2);
        checkBox_displayPlots->setObjectName(QString::fromUtf8("checkBox_displayPlots"));
        checkBox_displayPlots->setFont(font2);
        checkBox_displayPlots->setChecked(true);

        verticalLayout_2->addWidget(checkBox_displayPlots);

        checkBox_AutoDetectPorts = new QCheckBox(layoutWidget2);
        checkBox_AutoDetectPorts->setObjectName(QString::fromUtf8("checkBox_AutoDetectPorts"));
        checkBox_AutoDetectPorts->setFont(font2);
        checkBox_AutoDetectPorts->setChecked(true);

        verticalLayout_2->addWidget(checkBox_AutoDetectPorts);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(6);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        pushButton_start = new QPushButton(layoutWidget2);
        pushButton_start->setObjectName(QString::fromUtf8("pushButton_start"));
        sizePolicy3.setHeightForWidth(pushButton_start->sizePolicy().hasHeightForWidth());
        pushButton_start->setSizePolicy(sizePolicy3);
        QFont font3;
        font3.setPointSize(12);
        font3.setBold(true);
        font3.setWeight(75);
        pushButton_start->setFont(font3);

        verticalLayout->addWidget(pushButton_start);

        pushButton_pause = new QPushButton(layoutWidget2);
        pushButton_pause->setObjectName(QString::fromUtf8("pushButton_pause"));
        pushButton_pause->setFont(font3);

        verticalLayout->addWidget(pushButton_pause);

        pushButton_stop = new QPushButton(layoutWidget2);
        pushButton_stop->setObjectName(QString::fromUtf8("pushButton_stop"));
        sizePolicy3.setHeightForWidth(pushButton_stop->sizePolicy().hasHeightForWidth());
        pushButton_stop->setSizePolicy(sizePolicy3);
        pushButton_stop->setFont(font3);

        verticalLayout->addWidget(pushButton_stop);

        pushButton_settings = new QPushButton(layoutWidget2);
        pushButton_settings->setObjectName(QString::fromUtf8("pushButton_settings"));
        sizePolicy3.setHeightForWidth(pushButton_settings->sizePolicy().hasHeightForWidth());
        pushButton_settings->setSizePolicy(sizePolicy3);
        pushButton_settings->setFont(font3);

        verticalLayout->addWidget(pushButton_settings);

        pushButton_Refresh = new QPushButton(layoutWidget2);
        pushButton_Refresh->setObjectName(QString::fromUtf8("pushButton_Refresh"));
        sizePolicy3.setHeightForWidth(pushButton_Refresh->sizePolicy().hasHeightForWidth());
        pushButton_Refresh->setSizePolicy(sizePolicy3);
        pushButton_Refresh->setFont(font3);

        verticalLayout->addWidget(pushButton_Refresh);

        label_user_UART_port = new QLabel(layoutWidget2);
        label_user_UART_port->setObjectName(QString::fromUtf8("label_user_UART_port"));

        verticalLayout->addWidget(label_user_UART_port);

        lineEdit_UART_port = new QLineEdit(layoutWidget2);
        lineEdit_UART_port->setObjectName(QString::fromUtf8("lineEdit_UART_port"));

        verticalLayout->addWidget(lineEdit_UART_port);

        label_user_data_port = new QLabel(layoutWidget2);
        label_user_data_port->setObjectName(QString::fromUtf8("label_user_data_port"));

        verticalLayout->addWidget(label_user_data_port);

        lineEdit_data_port = new QLineEdit(layoutWidget2);
        lineEdit_data_port->setObjectName(QString::fromUtf8("lineEdit_data_port"));

        verticalLayout->addWidget(lineEdit_data_port);


        verticalLayout_2->addLayout(verticalLayout);


        verticalLayout_7->addLayout(verticalLayout_2);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_7->addItem(verticalSpacer_2);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        label_graph_settings = new QLabel(layoutWidget2);
        label_graph_settings->setObjectName(QString::fromUtf8("label_graph_settings"));
        label_graph_settings->setFont(font);

        verticalLayout_4->addWidget(label_graph_settings);

        gridLayout_graph_settings = new QGridLayout();
        gridLayout_graph_settings->setSpacing(6);
        gridLayout_graph_settings->setObjectName(QString::fromUtf8("gridLayout_graph_settings"));
        label_dropdownlist = new QLabel(layoutWidget2);
        label_dropdownlist->setObjectName(QString::fromUtf8("label_dropdownlist"));
        sizePolicy1.setHeightForWidth(label_dropdownlist->sizePolicy().hasHeightForWidth());
        label_dropdownlist->setSizePolicy(sizePolicy1);
        QFont font4;
        font4.setFamily(QString::fromUtf8("Arial"));
        font4.setPointSize(10);
        font4.setBold(true);
        font4.setWeight(75);
        label_dropdownlist->setFont(font4);
        label_dropdownlist->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_graph_settings->addWidget(label_dropdownlist, 1, 0, 1, 1);

        graphpatient_dropdownlist = new QComboBox(layoutWidget2);
        graphpatient_dropdownlist->addItem(QString());
        graphpatient_dropdownlist->addItem(QString());
        graphpatient_dropdownlist->addItem(QString());
        graphpatient_dropdownlist->addItem(QString());
        graphpatient_dropdownlist->addItem(QString());
        graphpatient_dropdownlist->addItem(QString());
        graphpatient_dropdownlist->setObjectName(QString::fromUtf8("graphpatient_dropdownlist"));
        graphpatient_dropdownlist->setFont(font4);

        gridLayout_graph_settings->addWidget(graphpatient_dropdownlist, 1, 1, 1, 1);


        verticalLayout_4->addLayout(gridLayout_graph_settings);


        verticalLayout_7->addLayout(verticalLayout_4);

        verticalSpacer_6 = new QSpacerItem(20, 20, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_7->addItem(verticalSpacer_6);

        label_legend = new QLabel(layoutWidget2);
        label_legend->setObjectName(QString::fromUtf8("label_legend"));
        label_legend->setFont(font);

        verticalLayout_7->addWidget(label_legend);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setSpacing(6);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        label_legend_camera = new QLabel(layoutWidget2);
        label_legend_camera->setObjectName(QString::fromUtf8("label_legend_camera"));
        label_legend_camera->setFont(font4);
        label_legend_camera->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(label_legend_camera, 2, 0, 1, 1);

        label_legend_integrate = new QLabel(layoutWidget2);
        label_legend_integrate->setObjectName(QString::fromUtf8("label_legend_integrate"));
        label_legend_integrate->setFont(font4);
        label_legend_integrate->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(label_legend_integrate, 3, 0, 1, 1);

        label_legend_radar = new QLabel(layoutWidget2);
        label_legend_radar->setObjectName(QString::fromUtf8("label_legend_radar"));
        label_legend_radar->setFont(font4);
        label_legend_radar->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(label_legend_radar, 1, 0, 1, 1);

        widget_radar = new QWidget(layoutWidget2);
        widget_radar->setObjectName(QString::fromUtf8("widget_radar"));

        gridLayout_2->addWidget(widget_radar, 1, 1, 1, 1);

        widget_camera = new QWidget(layoutWidget2);
        widget_camera->setObjectName(QString::fromUtf8("widget_camera"));

        gridLayout_2->addWidget(widget_camera, 2, 1, 1, 1);

        widget_integrate = new QWidget(layoutWidget2);
        widget_integrate->setObjectName(QString::fromUtf8("widget_integrate"));

        gridLayout_2->addWidget(widget_integrate, 3, 1, 1, 1);


        verticalLayout_7->addLayout(gridLayout_2);

        verticalSpacer_5 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_7->addItem(verticalSpacer_5);

        splitter_2->addWidget(layoutWidget2);

        verticalLayout_6->addWidget(splitter_2);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1485, 22));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "Vital Master", nullptr));
        label->setText(QCoreApplication::translate("MainWindow", "Patient", nullptr));
        label_p3->setText(QCoreApplication::translate("MainWindow", "P3", nullptr));
        label_p0->setText(QCoreApplication::translate("MainWindow", "P0", nullptr));
        label_p4->setText(QCoreApplication::translate("MainWindow", "P4", nullptr));
        label_p1->setText(QCoreApplication::translate("MainWindow", "P1", nullptr));
        label_p5->setText(QCoreApplication::translate("MainWindow", "P5", nullptr));
        label_p2->setText(QCoreApplication::translate("MainWindow", "P2", nullptr));
        label_LCDs_radar->setText(QCoreApplication::translate("MainWindow", "Radar", nullptr));
        label_HR_radar->setText(QCoreApplication::translate("MainWindow", "HR", nullptr));
        label_RR_radar->setText(QCoreApplication::translate("MainWindow", "RR", nullptr));
        label_LCDs_camera->setText(QCoreApplication::translate("MainWindow", "Camera", nullptr));
        label_RR_camera->setText(QCoreApplication::translate("MainWindow", "RR", nullptr));
        label_HR_camera->setText(QCoreApplication::translate("MainWindow", "HR", nullptr));
        label_LCDs_integrate->setText(QCoreApplication::translate("MainWindow", "Integrate", nullptr));
        label_RR_integrate->setText(QCoreApplication::translate("MainWindow", "RR", nullptr));
        label_HR_integrate->setText(QCoreApplication::translate("MainWindow", "HR", nullptr));
        checkBox_xCorr->setText(QCoreApplication::translate("MainWindow", "Use Time-Domain", nullptr));
        checkBox_FFT->setText(QCoreApplication::translate("MainWindow", "Use Freq-Domain", nullptr));
        checkBox_SaveData->setText(QCoreApplication::translate("MainWindow", "Save Data", nullptr));
        checkBox_displayPlots->setText(QCoreApplication::translate("MainWindow", "Display Plots", nullptr));
        checkBox_AutoDetectPorts->setText(QCoreApplication::translate("MainWindow", "Auto-Detect COM Ports", nullptr));
        pushButton_start->setText(QCoreApplication::translate("MainWindow", "Start", nullptr));
        pushButton_pause->setText(QCoreApplication::translate("MainWindow", "Pause", nullptr));
        pushButton_stop->setText(QCoreApplication::translate("MainWindow", "Stop", nullptr));
        pushButton_settings->setText(QCoreApplication::translate("MainWindow", "Settings", nullptr));
        pushButton_Refresh->setText(QCoreApplication::translate("MainWindow", "Refresh", nullptr));
        label_user_UART_port->setText(QCoreApplication::translate("MainWindow", "User UART COM Port", nullptr));
        label_user_data_port->setText(QCoreApplication::translate("MainWindow", "Data COM Port", nullptr));
        label_graph_settings->setText(QCoreApplication::translate("MainWindow", "Graph Settings:", nullptr));
        label_dropdownlist->setText(QCoreApplication::translate("MainWindow", "Patient:", nullptr));
        graphpatient_dropdownlist->setItemText(0, QCoreApplication::translate("MainWindow", "P0", nullptr));
        graphpatient_dropdownlist->setItemText(1, QCoreApplication::translate("MainWindow", "P1", nullptr));
        graphpatient_dropdownlist->setItemText(2, QCoreApplication::translate("MainWindow", "P2", nullptr));
        graphpatient_dropdownlist->setItemText(3, QCoreApplication::translate("MainWindow", "P3", nullptr));
        graphpatient_dropdownlist->setItemText(4, QCoreApplication::translate("MainWindow", "P4", nullptr));
        graphpatient_dropdownlist->setItemText(5, QCoreApplication::translate("MainWindow", "P5", nullptr));

        label_legend->setText(QCoreApplication::translate("MainWindow", "Legend:", nullptr));
        label_legend_camera->setText(QCoreApplication::translate("MainWindow", "Camera", nullptr));
        label_legend_integrate->setText(QCoreApplication::translate("MainWindow", "Integrate", nullptr));
        label_legend_radar->setText(QCoreApplication::translate("MainWindow", "Radar", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H

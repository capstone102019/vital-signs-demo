#include <QDebug>
#include <iostream>
#include "mainwindow.h"
#include <QApplication>
#include "rt.h"
#include "rt.cpp"


#include <QtGui>
#if QT_VERSION >= QT_VERSION_CHECK(5,0,0)
#include <QtWidgets>
#endif



#define RADAR_PIPELINE "RADAR_PIPELINE"

struct pipelineStructure
{
    char respirationRate[12];
    float heartRate;
};

float respirationRate;
float heartRate;

UINT __stdcall RadarThread(void *args)
{

    HANDLE hPipe;
    char buffer[1024];
    DWORD dwRead;


    hPipe = CreateNamedPipe(TEXT("\\\\.\\pipe\\python_pipe"),
        PIPE_ACCESS_DUPLEX,
        PIPE_TYPE_BYTE | PIPE_READMODE_BYTE | PIPE_WAIT,   // FILE_FLAG_FIRST_PIPE_INSTANCE is not needed but forces CreateNamedPipe(..) to fail if the pipe already exists...
        1,
        1024 * 16,
        1024 * 16,
        NMPWAIT_USE_DEFAULT_WAIT,
        NULL);
    while (hPipe != INVALID_HANDLE_VALUE)
    {
        if (ConnectNamedPipe(hPipe, NULL) != FALSE)   // wait for someone to connect to the pipe
        {
            while (ReadFile(hPipe, buffer, sizeof(buffer) - 1, &dwRead, NULL) != FALSE)
            {
                /* add terminating zero */
                buffer[dwRead] = '\0';

                /* do something with data in buffer */
                printf("%s", buffer);
            }
        }

        DisconnectNamedPipe(hPipe);
    }

    return 0 ;
}

// Source for OverlayWidget and GridOverlay: https://github.com/KubaO/stackoverflown/tree/master/questions/overlay-widget-19362455
// Note: The author of this code states "All my original contributions to StackOverflow are hereby placed into the public domain"
class OverlayWidget : public QWidget
{
   void newParent() {
      if (!parent()) return;
      parent()->installEventFilter(this);
      raise();
   }
public:
   explicit OverlayWidget(QWidget * parent = {}) : QWidget{parent} {
      setAttribute(Qt::WA_NoSystemBackground);
      setAttribute(Qt::WA_TransparentForMouseEvents);
      newParent();
   }
protected:
   //! Catches resize and child events from the parent widget
   bool eventFilter(QObject * obj, QEvent * ev) override {
      if (obj == parent()) {
         if (ev->type() == QEvent::Resize)
            resize(static_cast<QResizeEvent*>(ev)->size());
         else if (ev->type() == QEvent::ChildAdded)
            raise();
      }
      return QWidget::eventFilter(obj, ev);
   }
   //! Tracks parent widget changes
   bool event(QEvent* ev) override {
      if (ev->type() == QEvent::ParentAboutToChange) {
         if (parent()) parent()->removeEventFilter(this);
      }
      else if (ev->type() == QEvent::ParentChange)
         newParent();
      return QWidget::event(ev);
   }
};

class GridOverlay : public OverlayWidget
{
public:
   GridOverlay(QWidget * parent = {}) : OverlayWidget{parent} {
      setAttribute(Qt::WA_NoSystemBackground);
      setAttribute(Qt::WA_TransparentForMouseEvents);
   }
protected:
   void paintEvent(QPaintEvent *) override {

      // Get current window dimensions
      int side = qMin(width(), height());

      QPainter painter{this};

      /*
      // This grid does not resize (it might look wrong with your computer screen's max window dimensions)
      // Section Outlines
      painter.setPen(QPen(Qt::gray, 2));
      painter.drawRect(5, 20, 1145, 305);
      // painter.drawRect(5, 335, 1150, 390);
      // Vertical Lines in LCD section
      painter.setPen(QPen(Qt::lightGray, 1));
      painter.drawLine(68, 20, 68, 325);   // Line between Patient and Radar
      painter.drawLine(428, 20, 428, 325); // Line between Radar and Camera
      painter.drawLine(790, 20, 790, 325); // Line between Camera and Integrate
      // Horizontal Lines in LCD section
      painter.setPen(QPen(Qt::lightGray, 1));
      painter.drawLine(5, 52, 1150, 52);   // Line between titles and section titles
      painter.drawLine(5, 78, 1150, 78);   // Line between section titles and LCDS
      painter.drawLine(5, 120, 1150, 120); // Line between p0 and p1
      painter.drawLine(5, 160, 1150, 160); // Line between p1 and p2
      painter.drawLine(5, 200, 1150, 200); // Line between p2 and p3
      painter.drawLine(5, 240, 1150, 240); // Line between p3 and p4
      painter.drawLine(5, 280, 1150, 280); // Line between p4 and p5
      */

      /*
       *  This grid resizes properly. If for some reason it is inaccurate, remove the grid by
       *  commenting out the following in main:
       *       GridOverlay overlay{&window};
       *       overlay.show();
       * Note: The grid makes the LCD table more obvious. This was implemented using code because QT Designer (Forms->mainwindow.ui)
       *       does not let you give borders to layouts. More functionality is available through coding it in C++, although it can also
       *       be more time-consuming and requires more effort. In the future, as time permits, coding the GUI from scratch would make
       *       it more robust. The solution presented here is the following: A transparent layer is created on top of the QT Designer GUI.
       *       This layer is transparent to mouse clicks and only includes paintEvents (i.e. lines and boxes). Since different widgets in
       *       the main GUI have different sizePolicies (fixed, expanding, minimum, etc.), the simplest solution was to draw a line, run the
       *       program, check the position of the line, and adjust as necessary. Checks were made for resizing of the program window, such that
       *       the grid always matches up with the LCD table exactly. There may be better solutions, but in the interest of time, this was sufficient. - Dalton
       */

      // Section Outlines
      painter.setPen(QPen(Qt::gray, 2));
      painter.drawRect(width()*0.0042, height()*0.028, 0.9698*width()-175.32, height()*0.41); // 3rd param: (width()-180)*0.974-0.0042*width()

      // Vertical Lines in LCD section
      painter.setPen(QPen(Qt::lightGray, 1));
      painter.drawLine(70-width()*0.002, height()*0.028, 70-width()*0.002, height()*0.436);         // Line between Patient and Radar
      painter.setPen(QPen(Qt::lightGray, 1));
      painter.drawLine((width()-95)*0.337, height()*0.028, (width()-95)*0.337, height()*0.436);     // Line between Radar and Camera
      painter.setPen(QPen(Qt::lightGray, 1));
      painter.drawLine((width()-180)*0.6662, height()*0.028, (width()-180)*0.6662, height()*0.436); // Line between Camera and Integrate

      // Horizontal Lines in LCD section
      painter.setPen(QPen(Qt::lightGray, 1));
      painter.drawLine(width()*0.0042, height()*0.07, (width()-180)*0.972, height()*0.07);   // Line between titles and section titles
      painter.drawLine(width()*0.0042, height()*0.105, (width()-180)*0.972, height()*0.105); // Line between section titles and LCDS
      painter.drawLine(width()*0.0042, height()*0.162, (width()-180)*0.972, height()*0.162); // Line between p0 and p1
      painter.drawLine(width()*0.0042, height()*0.215, (width()-180)*0.972, height()*0.215); // Line between p1 and p2
      painter.drawLine(width()*0.0042, height()*0.269, (width()-180)*0.972, height()*0.269); // Line between p2 and p3
      painter.drawLine(width()*0.0042, height()*0.323, (width()-180)*0.972, height()*0.323); // Line between p3 and p4
      painter.drawLine(width()*0.0042, height()*0.376, (width()-180)*0.972, height()*0.376); // Line between p4 and p5
   }
};

int main(int argc, char *argv[])
{
    QApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication a(argc, argv);
    MainWindow window;

    GridOverlay overlay{&window}; // Transparent overlay on top of main GUI
    overlay.show();

    window.show();

    cout << "Creating radar child thread.\n";
    CThread	radarThread(RadarThread,			// name of function representing the thread
                   ACTIVE,				// thread is created in ACTIVE state (could use SUSPENDED)
                   NULL					// Pass no arguments to the thread function
    );

    //radarThread.WaitForThread() ;		// wait for the child process to Terminate

    return a.exec();
}

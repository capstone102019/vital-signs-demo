#-------------------------------------------------
#
# Project created by QtCreator 2017-05-31T16:53:16
#
#-------------------------------------------------

QT       += core gui
QT       += core serialport

CONFIG   += console

LIBS += -lOpenGL32

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets  printsupport

TARGET = VitalMaster
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
DEFINES += QCUSTOMPLOT_USE_OPENGL
DEFINES += _MBCS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES +=\
    main.cpp \
    mainwindow.cpp \
    dialogsettings.cpp \
    qcustomplot.cpp \
    rt.cpp

HEADERS  += mainwindow.h \
    client/client.h \
    dialogsettings.h \
    qcustomplot.h \
    rt.h

FORMS    += mainwindow.ui \
    dialogsettings.ui

DISTFILES +=

RESOURCES += \
    logo.qrc \

INCLUDEPATH += "C:/Program Files (x86)/Windows Kits/10/Include/10.0.17763.0/ucrt" \
"C:/Program Files (x86)/Windows Kits/10/Include/10.0.17763.0/um" \
"C:/Program Files (x86)/Windows Kits/10/Include/10.0.17763.0/shared"


LIBS += -L"C:/Program Files (x86)/Windows Kits/10/Lib/10.0.17763.0/ucrt/x86" \
-L"C:/Program Files (x86)/Windows Kits/10/Lib/10.0.17763.0/um/x86"

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QtEndian>
#include <QDialog>
#include <QSerialPortInfo>
#include <QFile>
#include <QElapsedTimer>                       // This class provides a fast way to calculate elapsed times
#include "dialogsettings.h"
#include "rt.h"

#define LENGTH_MAGIC_WORD_BYTES         8  // Length of Magic Word appended to the UART packet from the EVM

#define LENGTH_HEADER_BYTES               40   // Header + Magic Word
#define LENGTH_TLV_MESSAGE_HEADER_BYTES   8
#define LENGTH_DEBUG_DATA_OUT_BYTES       128   // VitalSignsDemo_OutputStats size
#define MMWDEMO_OUTPUT_MSG_SEGMENT_LEN    32   // The data sent out through the UART has Extra Padding to make it a
                                                   // multiple of MMWDEMO_OUTPUT_MSG_SEGMENT_LEN
#define LENGTH_OFFSET_BYTES               LENGTH_HEADER_BYTES  - LENGTH_MAGIC_WORD_BYTES + LENGTH_TLV_MESSAGE_HEADER_BYTES
#define LENGTH_OFFSET_NIBBLES             2*LENGTH_OFFSET_BYTES

#define  INDEX_GLOBAL_COUNT                  6
#define  INDEX_RANGE_BIN_PHASE               1
#define  INDEX_RANGE_BIN_VALUE               2
#define  INDEX_PHASE                         5
#define  INDEX_BREATHING_WAVEFORM            6
#define  INDEX_HEART_WAVEFORM                7
#define  INDEX_HEART_RATE_EST_FFT            8
#define  INDEX_HEART_RATE_EST_FFT_4Hz        9
#define  INDEX_HEART_RATE_EST_FFT_xCorr      10
#define  INDEX_HEART_RATE_EST_PEAK           11
#define  INDEX_BREATHING_RATE_FFT            12
#define  INDEX_BREATHING_RATE_xCorr          13
#define  INDEX_BREATHING_RATE_PEAK           14
#define  INDEX_CONFIDENCE_METRIC_BREATH      15
#define  INDEX_CONFIDENCE_METRIC_BREATH_xCorr 16
#define  INDEX_CONFIDENCE_METRIC_HEART       17
#define  INDEX_CONFIDENCE_METRIC_HEART_4Hz   18
#define  INDEX_CONFIDENCE_METRIC_HEART_xCorr 19
#define  INDEX_ENERGYWFM_BREATH              20
#define  INDEX_ENERGYWFM_HEART               21
#define  INDEX_MOTION_DETECTION              22
#define  INDEX_BREATHING_RATE_HARM_ENERG     23
#define  INDEX_HEART_RATE_HARM_ENERG         24

#define  INDEX_RANGE_PROFILE_START           35//(LENGTH_DEBUG_DATA_OUT_BYTES+LENGTH_TLV_MESSAGE_HEADER_BYTES)/4  + 1

#define  INDEX_IN_GLOBAL_FRAME_COUNT                INDEX_GLOBAL_COUNT*8
#define  INDEX_IN_RANGE_BIN_INDEX                   LENGTH_MAGIC_WORD_BYTES + LENGTH_OFFSET_NIBBLES + INDEX_RANGE_BIN_PHASE*8 + 4
#define  INDEX_IN_DATA_CONFIDENCE_METRIC_HEART_4Hz  LENGTH_MAGIC_WORD_BYTES + LENGTH_OFFSET_NIBBLES + INDEX_CONFIDENCE_METRIC_HEART_4Hz*8
#define  INDEX_IN_DATA_CONFIDENCE_METRIC_HEART_xCorr  LENGTH_MAGIC_WORD_BYTES + LENGTH_OFFSET_NIBBLES + INDEX_CONFIDENCE_METRIC_HEART_xCorr*8
#define  INDEX_IN_DATA_PHASE                        LENGTH_MAGIC_WORD_BYTES + LENGTH_OFFSET_NIBBLES + INDEX_PHASE*8
#define  INDEX_IN_DATA_BREATHING_WAVEFORM           LENGTH_MAGIC_WORD_BYTES + LENGTH_OFFSET_NIBBLES + INDEX_BREATHING_WAVEFORM*8
#define  INDEX_IN_DATA_HEART_WAVEFORM               LENGTH_MAGIC_WORD_BYTES + LENGTH_OFFSET_NIBBLES + INDEX_HEART_WAVEFORM*8
#define  INDEX_IN_DATA_BREATHING_RATE_FFT           LENGTH_MAGIC_WORD_BYTES + LENGTH_OFFSET_NIBBLES + INDEX_BREATHING_RATE_FFT*8
#define  INDEX_IN_DATA_HEART_RATE_EST_FFT           LENGTH_MAGIC_WORD_BYTES + LENGTH_OFFSET_NIBBLES + INDEX_HEART_RATE_EST_FFT*8
#define  INDEX_IN_DATA_HEART_RATE_EST_FFT_4Hz       LENGTH_MAGIC_WORD_BYTES + LENGTH_OFFSET_NIBBLES + INDEX_HEART_RATE_EST_FFT_4Hz*8
#define  INDEX_IN_DATA_HEART_RATE_EST_FFT_xCorr     LENGTH_MAGIC_WORD_BYTES + LENGTH_OFFSET_NIBBLES + INDEX_HEART_RATE_EST_FFT_xCorr*8
#define  INDEX_IN_DATA_BREATHING_RATE_PEAK          LENGTH_MAGIC_WORD_BYTES + LENGTH_OFFSET_NIBBLES + INDEX_BREATHING_RATE_PEAK*8
#define  INDEX_IN_DATA_HEART_RATE_EST_PEAK          LENGTH_MAGIC_WORD_BYTES + LENGTH_OFFSET_NIBBLES + INDEX_HEART_RATE_EST_PEAK*8
#define  INDEX_IN_DATA_CONFIDENCE_METRIC_BREATH     LENGTH_MAGIC_WORD_BYTES + LENGTH_OFFSET_NIBBLES + INDEX_CONFIDENCE_METRIC_BREATH*8
#define  INDEX_IN_DATA_CONFIDENCE_METRIC_HEART      LENGTH_MAGIC_WORD_BYTES + LENGTH_OFFSET_NIBBLES + INDEX_CONFIDENCE_METRIC_HEART*8
#define  INDEX_IN_DATA_ENERGYWFM_BREATH             LENGTH_MAGIC_WORD_BYTES + LENGTH_OFFSET_NIBBLES + INDEX_ENERGYWFM_BREATH*8
#define  INDEX_IN_DATA_ENERGYWFM_HEART              LENGTH_MAGIC_WORD_BYTES + LENGTH_OFFSET_NIBBLES + INDEX_ENERGYWFM_HEART*8
#define  INDEX_IN_DATA_MOTION_DETECTION_FLAG           LENGTH_MAGIC_WORD_BYTES + LENGTH_OFFSET_NIBBLES  + INDEX_MOTION_DETECTION*8
#define  INDEX_IN_DATA_CONFIDENCE_METRIC_BREATH_xCorr  LENGTH_MAGIC_WORD_BYTES + LENGTH_OFFSET_NIBBLES  + INDEX_CONFIDENCE_METRIC_BREATH_xCorr*8
#define  INDEX_IN_DATA_BREATHING_RATE_HARM_ENERGY   LENGTH_MAGIC_WORD_BYTES + LENGTH_OFFSET_NIBBLES + INDEX_BREATHING_RATE_HARM_ENERG*8
#define  INDEX_IN_DATA_BREATHING_RATE_xCorr         LENGTH_MAGIC_WORD_BYTES + LENGTH_OFFSET_NIBBLES + INDEX_BREATHING_RATE_xCorr*8

#define  INDEX_IN_DATA_RANGE_PROFILE_START          LENGTH_MAGIC_WORD_BYTES + LENGTH_OFFSET_NIBBLES + INDEX_RANGE_PROFILE_START*8

#define NUM_PTS_DISTANCE_TIME_PLOT        (256)
#define HEART_RATE_EST_MEDIAN_FLT_SIZE    (200)
#define HEART_RATE_EST_FINAL_OUT_SIZE     (200)
#define THRESH_HEART_CM                   (0.25)
#define THRESH_BREATH_CM                  (1.0)
#define BACK_THRESH_BPM                   (4)
#define BACK_THRESH_CM                    (0.20)
#define BACK_THRESH_4Hz_CM                (0.15)
#define THRESH_BACK                       (30)
#define THRESH_DIFF_EST                   (20)
#define ALPHA_HEARTRATE_CM                (0.2)
#define ALPHA_RCS                         (0.2)
#define APPLY_KALMAN_FILTER               (0.0)

// Additional Variables
#define THRESH_RCS                         500
#define RADAR_PIPELINE                    "RADAR_PIPELINE"

bool radarPosition = 0;   //0 for front, 1 for back
bool loadConfig    = 1;   //0 for don't load, 1 for load

// Radar variables
float radar_HR_p0, radar_RR_p0;

// Camera variables. Set the following to the HR and RR from the Camera
float camera_HR_p0 = 20;//01;
float camera_RR_p0 = 10;//02;
float camera_HR_p1 = 11;
float camera_RR_p1 = 12;
float camera_HR_p2 = 21;
float camera_RR_p2 = 22;
float camera_HR_p3 = 31;
float camera_RR_p3 = 32;
float camera_HR_p4 = 41;
float camera_RR_p4 = 42;
float camera_HR_p5 = 51;
float camera_RR_p5 = 52;


/*
//Test with random number generator
QRandomGenerator gen1 = QRandomGenerator();
float camera_HR_p0 = (float)gen1.bounded(50, 120);
float camera_RR_p0 = (float)gen1.bounded(10, 30);
float camera_HR_p1 = (float)gen1.bounded(50, 120);
float camera_RR_p1 = (float)gen1.bounded(10, 30);
float camera_HR_p2 = (float)gen1.bounded(50, 120);
float camera_RR_p2 = (float)gen1.bounded(10, 30);
float camera_HR_p3 = (float)gen1.bounded(50, 120);
float camera_RR_p3 = (float)gen1.bounded(10, 30);
float camera_HR_p4 = (float)gen1.bounded(50, 120);
float camera_RR_p4 = (float)gen1.bounded(10, 30);
float camera_HR_p5 = (float)gen1.bounded(50, 120);
float camera_RR_p5 = (float)gen1.bounded(10, 30);
*/

// Integrate variables (Test: take the sum)
float integrate_HR_p0 = (radar_HR_p0+camera_HR_p0);
float integrate_RR_p0 = (radar_RR_p0+camera_RR_p0);

// Variables to indicate a detected person (0 for not detected, 1 for detected)
bool detected_p0 = 1;
bool detected_p1 = 0;
bool detected_p2 = 1;
bool detected_p3 = 0;
bool detected_p4 = 0;
bool detected_p5 = 1;

// Limits for HR and RR graphs
float HR_PLOT_MAX_YAXIS = 200.0;
float HR_PLOT_MIN_YAXIS = -5.0;
float RR_PLOT_MAX_YAXIS = 50.0;
float RR_PLOT_MIN_YAXIS = -5.0;

QSerialPort *serialRead, *serialWrite;
bool  FileSavingFlag;                   // "True" if we want to save the data on to a text file
bool  serialPortFound_Flag ;            // "True" if Serial port found
bool  FlagSerialPort_Connected, dataPort_Connected, userPort_Connected;
float thresh_breath = 10;
float thresh_heart = 0.1F;


// GUI Status
enum  gui_status { gui_running, gui_stopped, gui_paused };
gui_status current_gui_status;

QSettings settings("Texas Instruments", "Vital Signs");
QFile outFile("dataOutputFromEVM.bin");     // File to save the Data

struct pipelineStructure {
    float respirationRate;
    float heartRate;
};

CPipe radarPipeline(RADAR_PIPELINE, 1024);

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    /*
    // Displays Logo
    QPixmap pic(":/ti_logo_trimmed.PNG");
    if(pic.isNull())
    {
    qDebug()<<"logo not Found";
    }
    else
    {
    ui->pushButton_Icon->setIcon(QIcon(pic));
    }
    */

    localCount = 0;
    currIndex  = 0;

    qDebug() <<"QT version = " <<QT_VERSION_STR;

    qint32 baudRate = 921600;
    FLAG_PAUSE = false;
    outFile.open(QIODevice::Append);  // Open the file by default
    AUTO_DETECT_COM_PORTS = ui->checkBox_AutoDetectPorts->isChecked();


 if(AUTO_DETECT_COM_PORTS)
   {
    /* Serial Port Settings */
     serialPortFound_Flag = serialPortFind();
     if (serialPortFound_Flag)
     {
        qDebug()<<"Serial Port Found";
        qDebug()<<"Data Port Number is" << dataPortNum;
        qDebug()<<"User Port Number is" << userPortNum;
     }
    }

     serialRead = new QSerialPort(this);
     dataPort_Connected = serialPortConfig(serialRead, baudRate, dataPortNum);
     if (dataPort_Connected)
         printf("Data port succesfully Open\n");
     else
         printf("data port did Not Open\n");

     serialWrite = new QSerialPort(this);
     userPort_Connected = serialPortConfig(serialWrite, 115200, userPortNum);
     if (userPort_Connected)
         printf("User port succesfully Open\n");
     else
         printf("User port did not Open \n");

     connect(serialRead,SIGNAL(readyRead()),this,SLOT(serialRecieved()));

    // Plot Settings
    QFont font;
    font.setPointSize(12);
    QPen radarPen;
    radarPen.setWidth(1.5);    // Width greater than 1 considerably slows down the application
    radarPen.setColor(Qt::blue);
    QPen cameraPen;
    cameraPen.setWidth(1.5);
    cameraPen.setColor(Qt::red);
    QPen integratePen;
    integratePen.setWidth(3);
    integratePen.setColor(Qt::magenta);
    QFont titleFont;
    titleFont.setPointSize(15);
    titleFont.bold();

    statusBar()->showMessage(tr("Ready"));

    xDistTimePlot.resize(NUM_PTS_DISTANCE_TIME_PLOT);
    yDistTimePlot.resize(NUM_PTS_DISTANCE_TIME_PLOT);
    HR_radar_buffer.resize(NUM_PTS_DISTANCE_TIME_PLOT);
    RR_radar_buffer.resize(NUM_PTS_DISTANCE_TIME_PLOT);
    HR_camera_buffer.resize(NUM_PTS_DISTANCE_TIME_PLOT);
    RR_camera_buffer.resize(NUM_PTS_DISTANCE_TIME_PLOT);
    HR_integrate_buffer.resize(NUM_PTS_DISTANCE_TIME_PLOT);
    RR_integrate_buffer.resize(NUM_PTS_DISTANCE_TIME_PLOT);

    ui->RR_plot->addGraph(0);
    ui->RR_plot->setBackground(this->palette().background().color());
    ui->RR_plot->axisRect()->setBackground(this->palette().background().color());
    ui->RR_plot->xAxis->setLabelFont(font);
    ui->RR_plot->yAxis->setLabel("Respiratory Rate (Breath Per Minute)");
    ui->RR_plot->yAxis->setLabelFont(font);
    ui->RR_plot->axisRect()->setupFullAxesBox();
    ui->RR_plot->xAxis->setRange(0,NUM_PTS_DISTANCE_TIME_PLOT);
    ui->RR_plot->yAxis->setRange(RR_PLOT_MIN_YAXIS,RR_PLOT_MAX_YAXIS);
    ui->RR_plot->graph(0)->setPen(radarPen);
    ui->RR_plot->addGraph();
    ui->RR_plot->graph(1)->setPen(cameraPen);
    ui->RR_plot->addGraph();
    ui->RR_plot->graph(2)->setPen(integratePen);

    QCPTextElement *myTitle_BreathWfm = new QCPTextElement(ui->RR_plot, "       Respiratory Rate");
    myTitle_BreathWfm->setFont(titleFont);
    ui->RR_plot->plotLayout()->insertRow(0); // inserts an empty row above the default axis rect
    ui->RR_plot->plotLayout()->addElement(0, 0, myTitle_BreathWfm);

    ui->HR_plot->addGraph(0);
    ui->HR_plot->setBackground(this->palette().background().color());
    ui->HR_plot->axisRect()->setBackground(this->palette().background().color());
    ui->HR_plot->xAxis->setLabelFont(font);
    ui->HR_plot->yAxis->setLabel("Heart Rate (Beats Per Minute)");
    ui->HR_plot->yAxis->setLabelFont(font);
    ui->HR_plot->axisRect()->setupFullAxesBox();
    ui->HR_plot->xAxis->setRange(0,NUM_PTS_DISTANCE_TIME_PLOT);
    ui->HR_plot->yAxis->setRange(RR_PLOT_MIN_YAXIS,HR_PLOT_MAX_YAXIS);
    ui->HR_plot->graph(0)->setPen(radarPen);
    ui->HR_plot->addGraph();
    ui->HR_plot->graph(1)->setPen(cameraPen);
    ui->HR_plot->addGraph();
    ui->HR_plot->graph(2)->setPen(integratePen);

    QCPTextElement *myTitle_HeartWfm = new QCPTextElement(ui->HR_plot, "       Heart Rate");
    myTitle_HeartWfm->setFont(titleFont);
    ui->HR_plot->plotLayout()->insertRow(0); // inserts an empty row above the default axis rect
    ui->HR_plot->plotLayout()->addElement(0, 0, myTitle_HeartWfm);

    QPalette LCD_on = palette();
    LCD_on.setColor(QPalette::Normal, QPalette::Window, Qt::white);

    QPalette LCD_off = palette();
    LCD_off.setColor(QPalette::Normal, QPalette::Window, Qt::lightGray);

    QPalette blueBar = palette();
    blueBar.setColor(QPalette::Normal, QPalette::Window, Qt::blue);

    QPalette redBar = palette();
    redBar.setColor(QPalette::Normal, QPalette::Window, Qt::red);

    QPalette magentaBar = palette();
    magentaBar.setColor(QPalette::Normal, QPalette::Window, Qt::magenta);

    // Legend Colors
    ui->widget_radar->setAutoFillBackground(true);
    ui->widget_radar->setPalette(blueBar);
    ui->widget_camera->setAutoFillBackground(true);
    ui->widget_camera->setPalette(redBar);
    ui->widget_integrate->setAutoFillBackground(true);
    ui->widget_integrate->setPalette(magentaBar);

    // Radar LCD
    ui->lcdNumber_radar_HR_p0->setAutoFillBackground(true);
    ui->lcdNumber_radar_HR_p0->setPalette(LCD_off);
    ui->lcdNumber_radar_RR_p0->setAutoFillBackground(true);
    ui->lcdNumber_radar_RR_p0->setPalette(LCD_off);

    // Camera LCD
    ui->lcdNumber_camera_HR_p0->setAutoFillBackground(true);
    ui->lcdNumber_camera_HR_p0->setPalette(LCD_off);
    ui->lcdNumber_camera_RR_p0->setAutoFillBackground(true);
    ui->lcdNumber_camera_RR_p0->setPalette(LCD_off);
    ui->lcdNumber_camera_HR_p1->setAutoFillBackground(true);
    ui->lcdNumber_camera_HR_p1->setPalette(LCD_off);
    ui->lcdNumber_camera_RR_p1->setAutoFillBackground(true);
    ui->lcdNumber_camera_RR_p1->setPalette(LCD_off);
    ui->lcdNumber_camera_HR_p2->setAutoFillBackground(true);
    ui->lcdNumber_camera_HR_p2->setPalette(LCD_off);
    ui->lcdNumber_camera_RR_p2->setAutoFillBackground(true);
    ui->lcdNumber_camera_RR_p2->setPalette(LCD_off);
    ui->lcdNumber_camera_HR_p3->setAutoFillBackground(true);
    ui->lcdNumber_camera_HR_p3->setPalette(LCD_off);
    ui->lcdNumber_camera_RR_p3->setAutoFillBackground(true);
    ui->lcdNumber_camera_RR_p3->setPalette(LCD_off);
    ui->lcdNumber_camera_HR_p4->setAutoFillBackground(true);
    ui->lcdNumber_camera_HR_p4->setPalette(LCD_off);
    ui->lcdNumber_camera_RR_p4->setAutoFillBackground(true);
    ui->lcdNumber_camera_RR_p4->setPalette(LCD_off);
    ui->lcdNumber_camera_HR_p5->setAutoFillBackground(true);
    ui->lcdNumber_camera_HR_p5->setPalette(LCD_off);
    ui->lcdNumber_camera_RR_p5->setAutoFillBackground(true);
    ui->lcdNumber_camera_RR_p5->setPalette(LCD_off);

    // Integrate LCD
    ui->lcdNumber_integrate_HR_p0->setAutoFillBackground(true);
    ui->lcdNumber_integrate_HR_p0->setPalette(LCD_off);
    ui->lcdNumber_integrate_RR_p0->setAutoFillBackground(true);
    ui->lcdNumber_integrate_RR_p0->setPalette(LCD_off);

    connect(this,SIGNAL(gui_statusChanged()),this,SLOT(gui_statusUpdate()));
}


void MainWindow::serialRecieved()
{
    QByteArray dataSerial = serialRead->readAll().toHex();
    int dataSize = dataSerial.size();
    dataBuffer = dataBuffer.append(dataSerial);
    indexBuffer = indexBuffer + dataSize;
    processData();
}


MainWindow::~MainWindow()
{
  serialWrite->write("sensorStop\n");
  serialWrite->waitForBytesWritten(10000);
  serialRead->close();
  serialWrite->close();
  delete ui;
}

void MainWindow::on_pushButton_start_clicked()
{
    localCount = 0;
    if(current_gui_status == gui_paused)
    {
        current_gui_status = gui_running;
        emit gui_statusChanged();
        return;
    }

    FLAG_PAUSE = false;
    AUTO_DETECT_COM_PORTS = ui->checkBox_AutoDetectPorts->isChecked();


   if (AUTO_DETECT_COM_PORTS)
    {
        if (!FlagSerialPort_Connected)
        {
            serialPortFound_Flag = serialPortFind();

            if( ! userPort_Connected)
            {
                userPort_Connected = serialPortConfig(serialWrite, 115200, userPortNum);
            }

            if( ! dataPort_Connected)
            {
                dataPort_Connected = serialPortConfig(serialRead, 921600, dataPortNum);
            }
        }
    }
    else
    {
        userPortNum = ui->lineEdit_UART_port->text();
        dataPortNum = ui->lineEdit_data_port->text();
        userPort_Connected = serialPortConfig(serialWrite, 115200, userPortNum);
        dataPort_Connected = serialPortConfig(serialRead, 921600, dataPortNum);
    }


    if (loadConfig == 1)
    {


   // Read the Configuration file path from the GUI

       QString profileBack =  "xwr1642_profile_VitalSigns_20fps_Back.cfg";
       qDebug() << "Configuration File Name Read from the GUI is %s/n"<<profileBack;


   DialogSettings myDialogue;
   QString userComPort = myDialogue.getUserComPortNum();
   qDebug()<<userComPort;
   QDir currDir = QCoreApplication::applicationDirPath();
   currDir.cdUp();
   QString filenameText = currDir.path();
   filenameText.append("/profiles/");        // Reads from this path

    if (radarPosition == 1)
    {
      filenameText.append("xwr1642_profile_VitalSigns_20fps_Back.cfg");        // Reads from this path
      qDebug() << "Configuration File Path is %s/n"<<filenameText;

      thresh_breath = 0.001F;
      thresh_heart  = 0.001F;
    }
    else
    {
      filenameText.append("xwr1642_profile_VitalSigns_20fps_Front.cfg");        // Reads from this path
      qDebug() << "Configuration File Path is %s/n"<<filenameText;
      thresh_breath = 10;
      thresh_heart = 0.1F;
    }

    QFile infile(filenameText);

    if (infile.open(QIODevice::ReadWrite))
    {
        QTextStream inStream(&infile);
        while (!inStream.atEnd())
        {
            QString line = inStream.readLine();  // Read a line from the input Text File
            qDebug() << line;
            serialWrite->write(line.toUtf8().constData());
            serialWrite->write("\n");
            serialWrite->waitForBytesWritten(10000);
            Sleep(1000);
        }
        infile.close();
    }

// Parse the Configuration Text File

    if (infile.open(QIODevice::ReadWrite))
    {
        QStringList listArgs;
        QString tempString;
        QTextStream inStream(&infile);
        while (!inStream.atEnd())
        {
            QString line = inStream.readLine();  // Read a line from the input Text File
            qDebug() << line;
            if (line.contains("vitalSignsCfg", Qt::CaseInsensitive) )
            {
                  listArgs = line.split(QRegExp("\\s+"));
                  tempString = listArgs.at(1);
                  demoParams.rangeStartMeters = tempString.toFloat();
                  qDebug() << demoParams.rangeStartMeters;

                  listArgs = line.split(QRegExp("\\s+"));
                  tempString = listArgs.at(2);
                  demoParams.rangeEndMeters = tempString.toFloat();
                  qDebug() << demoParams.rangeEndMeters;

                  listArgs = line.split(QRegExp("\\s+"));
                  tempString = listArgs.at(5);
                  demoParams.AGC_thresh = tempString.toFloat();
                  qDebug() << demoParams.AGC_thresh;
            }

            if (line.contains("profileCfg", Qt::CaseInsensitive) )
            {

                listArgs = line.split(QRegExp("\\s+"));
                tempString = listArgs.at(2);
                demoParams.stratFreq_GHz = tempString.toFloat();

                listArgs = line.split(QRegExp("\\s+"));
                tempString = listArgs.at(8);
                demoParams.freqSlope_MHZ_us = tempString.toFloat();

                listArgs = line.split(QRegExp("\\s+"));
                tempString = listArgs.at(10);
                demoParams.numSamplesChirp = tempString.toFloat();

                listArgs = line.split(QRegExp("\\s+"));
                tempString = listArgs.at(11);
                demoParams.samplingRateADC_ksps = tempString.toInt();
            }
        }
        infile.close();

        // Compute the Derived Configuration Parameters
        demoParams.chirpDuration_us = 1e3*demoParams.numSamplesChirp/demoParams.samplingRateADC_ksps;
        qDebug() << "Chirp Duration in us is :" << demoParams.chirpDuration_us;

        demoParams.chirpBandwidth_kHz = (demoParams.freqSlope_MHZ_us)*(demoParams.chirpDuration_us);
        qDebug() << "Chirp Bandwidth in kHz is : "<<demoParams.chirpBandwidth_kHz;

        float numTemp = (demoParams.chirpDuration_us)*(demoParams.samplingRateADC_ksps)*(3e8);
        float denTemp =  2*(demoParams.chirpBandwidth_kHz);
        demoParams.rangeMaximum_meters = numTemp/(denTemp*1e9);
        qDebug() << "Maximum Range in Meters is :"<< demoParams.rangeMaximum_meters;

        demoParams.rangeFFTsize = nextPower2(demoParams.numSamplesChirp);
        qDebug() << "Range-FFT size is :"<< demoParams.rangeFFTsize;

        demoParams.rangeBinSize_meters = demoParams.rangeMaximum_meters/demoParams.rangeFFTsize;
        qDebug() << "Range-Bin size is :"<< demoParams.rangeBinSize_meters;

        demoParams.rangeBinStart_index = demoParams.rangeStartMeters/demoParams.rangeBinSize_meters;
        demoParams.rangeBinEnd_index   = demoParams.rangeEndMeters/demoParams.rangeBinSize_meters;
        qDebug() << "Range-Bin Start Index is :"<< demoParams.rangeBinStart_index;
        qDebug() << "Range-Bin End Index is :"  << demoParams.rangeBinEnd_index;

        demoParams.numRangeBinProcessed = demoParams.rangeBinEnd_index - demoParams.rangeBinStart_index + 1;

        // Calculate the Payload size

        demoParams.totalPayloadSize_bytes = LENGTH_HEADER_BYTES;
        demoParams.totalPayloadSize_bytes += LENGTH_TLV_MESSAGE_HEADER_BYTES + (4*demoParams.numRangeBinProcessed);
        demoParams.totalPayloadSize_bytes += LENGTH_TLV_MESSAGE_HEADER_BYTES +  LENGTH_DEBUG_DATA_OUT_BYTES;
        qDebug() << "Total Payload size from the UART is :"<< demoParams.totalPayloadSize_bytes;

        if ((demoParams.totalPayloadSize_bytes % MMWDEMO_OUTPUT_MSG_SEGMENT_LEN) != 0)
        {
            int paddingFactor = ceil( (float) demoParams.totalPayloadSize_bytes/ (float) MMWDEMO_OUTPUT_MSG_SEGMENT_LEN);
            qDebug() << "Padding Factor is :"<<paddingFactor;

            demoParams.totalPayloadSize_bytes = MMWDEMO_OUTPUT_MSG_SEGMENT_LEN*paddingFactor;
        }

        qDebug() << "Total Payload size from the UART is :"<< demoParams.totalPayloadSize_bytes;
        demoParams.totalPayloadSize_nibbles = 2 * demoParams.totalPayloadSize_bytes;
       }
    }

    current_gui_status = gui_running;
    emit gui_statusChanged();
}

void MainWindow::on_pushButton_stop_clicked()
{
     serialWrite->write("sensorStop\n");
     loadConfig = 1;
     current_gui_status = gui_stopped;
     emit gui_statusChanged();
}


void MainWindow::on_pushButton_Refresh_clicked()
{
     serialWrite->write("guiMonitor 0 0 0 1\n");
     serialWrite->waitForBytesWritten(10000);
     localCount = 0;
     Sleep(2000);
}

void MainWindow::on_pushButton_settings_clicked()
{
    DialogSettings settingsDialogue;
    settingsDialogue.setModal(true);
    settingsDialogue.exec();
}


int MainWindow::nextPower2(int num)
{
    int power = 1;
    while(power < num)
        power*=2;
    return power;
}


float  MainWindow::parseValueFloat(QByteArray data, int valuePos, int valueSize)
{
    bool ok;
    QByteArray parseData;
    parseData = data.mid(valuePos,valueSize);
    QString strParseData = parseData;
    quint32 temp_int = strParseData.toUInt(&ok,16);
    temp_int =  qToBigEndian(temp_int);        // Convert to Big-Endian
    float parseValueOut;
    parseValueOut = *reinterpret_cast<float*>(&temp_int); // cast to Float
    return parseValueOut;
}

quint32  MainWindow::parseValueUint32(QByteArray data, int valuePos, int valueSize)
{
    bool ok;
    QByteArray parseData;
    parseData = data.mid(valuePos, valueSize);
    QString strParseData = parseData;
    quint32 tempInt32 = strParseData.toUInt(&ok,16);
    quint32 parseValueOut =  qToBigEndian(tempInt32);        // Convert to Big-Endian
    return parseValueOut;
}

quint16 MainWindow::parseValueUint16(QByteArray data, int valuePos, int valueSize)
{
    bool ok;
    QByteArray parseData;
    parseData = data.mid(valuePos, valueSize);
    QString strParseData = parseData;
    quint16 parseValueOut = strParseData.toInt(&ok,16);
    parseValueOut =  qToBigEndian(parseValueOut);        // Convert to Big-Endian
    return parseValueOut;
}

qint16  MainWindow::parseValueInt16(QByteArray data, int valuePos, int valueSize)
{
    bool ok;
    QByteArray parseData;
    parseData = data.mid(valuePos, valueSize);
    QString strParseData = parseData;
    qint16 parseValueOut = strParseData.toInt(&ok,16);
    parseValueOut =  qToBigEndian(parseValueOut);        // Convert to Big-Endian
    return parseValueOut;
}


bool MainWindow::serialPortFind()
{
    DialogSettings dialogBoxSerial;
    // Find available COM ports on the computer
    QString portNum;
        foreach (const QSerialPortInfo &serialPortInfo, QSerialPortInfo::availablePorts())
        {
        portNum = serialPortInfo.portName();
        dialogBoxSerial.setDataComPortNum(portNum);
        dialogBoxSerial.setUserComPortNum(portNum);
        }

    dataPortNum = dialogBoxSerial.getDataComPortNum();
    userPortNum = dialogBoxSerial.getUserComPortNum();
    ui->lineEdit_data_port->setText(dataPortNum);
    ui->lineEdit_UART_port->setText(userPortNum);
    if (portNum.isEmpty())
    return 0;
    else
    return 1;
}

bool MainWindow::serialPortConfig(QSerialPort *serial, qint32 baudRate, QString dataPortNum )
{
    serial->setPortName(dataPortNum);

    if(serial->open(QIODevice::ReadWrite) )
      {
      FlagSerialPort_Connected = 1;
      }
    else
      {
      FlagSerialPort_Connected = 0;
      return FlagSerialPort_Connected;
      }
    serial->setBaudRate(baudRate);
    serial->setDataBits(QSerialPort::Data8);
    serial->setParity(QSerialPort::NoParity);
    serial->setStopBits(QSerialPort::OneStop);
    serial->setFlowControl(QSerialPort::NoFlowControl);
    return FlagSerialPort_Connected;
}

void MainWindow::processData()
{
    QByteArray dataSave;
    QByteArray data;
    static float outHeartNew_CM;
    static float maxRCS_updated;
    bool MagicOk;

    // Kalman Filter
    static float Pk = 1;
    static float xk = 0;   // State Variable

    extern bool FileSavingFlag;
    FileSavingFlag = ui->checkBox_SaveData->isChecked();
    localCount = localCount + 1;

    while (dataBuffer.size() >= demoParams.totalPayloadSize_nibbles)
    {
    QElapsedTimer timer;       // For computing the time required for the operations
    timer.start();

    QByteArray searchStr("0201040306050807");  // Search String Array
    int dataStartIndex = dataBuffer.indexOf(searchStr);
    int indexTemp = localCount % NUM_PTS_DISTANCE_TIME_PLOT;

    if (dataStartIndex == -1)
    {
        MagicOk = 0;
        qDebug()<<" Magic Word Not Found --- local Count:  " <<localCount << "DataBufferSize" <<dataBuffer.size();
        break;
    }
    else
    {
        data       = dataBuffer.mid(dataStartIndex, demoParams.totalPayloadSize_nibbles /*TOTAL_PAYLOAD_SIZE_NIBBLES*/);
        dataBuffer = dataBuffer.remove(dataStartIndex, demoParams.totalPayloadSize_nibbles /*TOTAL_PAYLOAD_SIZE_NIBBLES*/);

        dataBuffer = dataBuffer.remove(0, dataStartIndex);
//      qDebug()<<"dataBuffer size after removal is "<<dataBuffer.size();

        // Check if all the data has been recieved succesfully
        if (data.size() >= demoParams.totalPayloadSize_nibbles)
        {
            MagicOk = 1;
            statusBar()->showMessage(tr("Sensor Running"));
        }
        else
        {
            MagicOk = 0;
            qDebug()<<" Data size is not OK --- local Count:  " <<localCount << "DataBufferSize" <<dataBuffer.size();
            qDebug()<< data.size();
            statusBar()->showMessage(tr("Frames are being missed. Please stop and Start the Program"));
        }
    }

  if (MagicOk == 1)
  {

   if(FileSavingFlag)
   {
       dataSave = data;
       outFile.write(dataSave.fromHex(data), demoParams.totalPayloadSize_bytes /*TOTAL_PAYLOAD_SIZE_BYTES*/);
   }

   quint32  globalCountOut     = parseValueUint32(data,40, 8);  // Global Count
   quint16  rangeBinIndexOut   = parseValueUint16(data,INDEX_IN_RANGE_BIN_INDEX, 4);  // parseValueUint16(data, 28, 4)
   float BreathingRate_FFT     = parseValueFloat(data, INDEX_IN_DATA_BREATHING_RATE_FFT, 8);  // Breathing Rate
   float BreathingRatePK_Out   = parseValueFloat(data, INDEX_IN_DATA_BREATHING_RATE_PEAK, 8);
   float heartRate_FFT         = parseValueFloat(data, INDEX_IN_DATA_HEART_RATE_EST_FFT, 8);  // Heart Rate
   float heartRate_Pk          = parseValueFloat(data, INDEX_IN_DATA_HEART_RATE_EST_PEAK, 8);
   float heartRate_xCorr       = parseValueFloat(data, INDEX_IN_DATA_HEART_RATE_EST_FFT_xCorr, 8);   // Heart Rate - AutoCorrelation
   float heartRate_FFT_4Hz     = parseValueFloat(data, INDEX_IN_DATA_HEART_RATE_EST_FFT_4Hz, 8)/2;   // Heart Rate - FFT-4Hz
   float phaseWfm_Out          = parseValueFloat(data, INDEX_IN_DATA_PHASE, 8); // Phase Waveforms
   float breathWfm_Out         = parseValueFloat(data, INDEX_IN_DATA_BREATHING_WAVEFORM, 8);   // Breathing Waveform
   float heartWfm_Out          = parseValueFloat(data, INDEX_IN_DATA_HEART_WAVEFORM, 8);   // Cardiac Waveform
   float breathRate_CM         = parseValueFloat(data, INDEX_IN_DATA_CONFIDENCE_METRIC_BREATH, 8);  // Confidence Metric
   float heartRate_CM          = parseValueFloat(data, INDEX_IN_DATA_CONFIDENCE_METRIC_HEART, 8);
   float heartRate_4Hz_CM         = parseValueFloat(data, INDEX_IN_DATA_CONFIDENCE_METRIC_HEART_4Hz, 8);
   float heartRate_xCorr_CM       = parseValueFloat(data, INDEX_IN_DATA_CONFIDENCE_METRIC_HEART_xCorr, 8);
   float outSumEnergyBreathWfm    = parseValueFloat(data, INDEX_IN_DATA_ENERGYWFM_BREATH, 8);  // Waveforms Energy
   float outSumEnergyHeartWfm     = parseValueFloat(data, INDEX_IN_DATA_ENERGYWFM_HEART, 8);  // Waveforms Energy
   float outMotionDetectionFlag   = parseValueFloat(data, INDEX_IN_DATA_MOTION_DETECTION_FLAG, 8);
   float BreathingRate_xCorr_CM   = parseValueFloat(data, INDEX_IN_DATA_CONFIDENCE_METRIC_BREATH_xCorr, 8);
   float BreathingRate_HarmEnergy = parseValueFloat(data, INDEX_IN_DATA_BREATHING_RATE_HARM_ENERGY, 8);
   float BreathingRate_xCorr = parseValueFloat(data, INDEX_IN_DATA_BREATHING_RATE_xCorr, 8);

   qDebug()<<"Frame Number is " << globalCountOut; // << " ; RadRR= " << radar_RR_p0 << " ; CamRR= " << camera_RR_p0 << " ; RadHR= " << radar_HR_p0 << " ; CamHR= " << camera_HR_p0;

   // Range Profile
   unsigned int numRangeBinProcessed = demoParams.rangeBinEnd_index - demoParams.rangeBinStart_index + 1;
   QVector<double> RangeProfile(2*numRangeBinProcessed);
   QVector<double> xRangePlot(numRangeBinProcessed), yRangePlot(numRangeBinProcessed);
   unsigned int indexRange = INDEX_IN_DATA_RANGE_PROFILE_START;/*168;*/

   for (unsigned int index = 0; index < 2*numRangeBinProcessed; index ++)
   {
   qint16  tempRange_int  = parseValueInt16(data, indexRange, 4);  // Range Bin Index
   RangeProfile[index] = tempRange_int;
   indexRange = indexRange + 4;
   }

   for (unsigned int indexRangeBin = 0; indexRangeBin < numRangeBinProcessed; indexRangeBin ++)
   {
      yRangePlot[indexRangeBin] = sqrt(RangeProfile[2*indexRangeBin]*RangeProfile[2*indexRangeBin]+ RangeProfile[2*indexRangeBin + 1]*RangeProfile[2*indexRangeBin + 1]);
      xRangePlot[indexRangeBin] = demoParams.rangeStartMeters + demoParams.rangeBinSize_meters*indexRangeBin;
   }
   double maxRCS = *std::max_element(yRangePlot.constBegin(), yRangePlot.constEnd());
   maxRCS_updated = ALPHA_RCS*(maxRCS) + (1-ALPHA_RCS)*maxRCS_updated;

   // make Decision
   float diffEst_heartRate, heartRateEstDisplay;

   float heartRate_OutMedian;
   static QVector<float> heartRateBuffer;
   heartRateBuffer.resize(HEART_RATE_EST_MEDIAN_FLT_SIZE);


   static QVector<float> heartRateOutBufferFinal;
   heartRateOutBufferFinal.resize(HEART_RATE_EST_FINAL_OUT_SIZE);


   // Heart-Rate Display Decision
   float outHeartPrev_CM = outHeartNew_CM;
   outHeartNew_CM = ALPHA_HEARTRATE_CM*(heartRate_CM) + (1-ALPHA_HEARTRATE_CM)*outHeartPrev_CM;

   diffEst_heartRate = abs(heartRate_FFT - heartRate_Pk);
   if ( (outHeartNew_CM > THRESH_HEART_CM) || (diffEst_heartRate < THRESH_DIFF_EST) )
   {
   heartRateEstDisplay = heartRate_FFT;
   }
   else
   {
   heartRateEstDisplay = heartRate_Pk;
   }

   if (ui->checkBox_xCorr->isChecked())
   {
       heartRateEstDisplay = heartRate_xCorr;
   }

   if (ui->checkBox_FFT->isChecked())
   {
       heartRateEstDisplay = heartRate_FFT;
   }

   if (radarPosition)
   {

      #ifdef HEAURITICS_APPROACH1

       if ( abs(heartRate_xCorr-heartRate_FFT) < THRESH_BACK)
       {
           heartRateEstDisplay = heartRate_FFT;
       }
       else
       {
           heartRateEstDisplay = heartRate_xCorr;
       }

       heartRateBuffer.insert(2*(localCount % HEART_RATE_EST_MEDIAN_FLT_SIZE/2), heartRateEstDisplay);


      if (ui->checkBox_FFT)
       {
           heartRateBuffer.insert(2*(localCount % HEART_RATE_EST_MEDIAN_FLT_SIZE/2)+1, heartRateEstDisplay);
       }
       else
       {
           heartRateBuffer.insert(2*(localCount % HEART_RATE_EST_MEDIAN_FLT_SIZE/2)+1, heartRate_FFT_4Hz);
       }
      #endif

      int IsvalueSelected = 0;

      if ( abs(heartRate_xCorr - 2*BreathingRate_FFT) > BACK_THRESH_BPM)
      {
          heartRateBuffer.insert(currIndex % HEART_RATE_EST_MEDIAN_FLT_SIZE, heartRate_xCorr);
          IsvalueSelected = 1;
          currIndex++;
      }
      if ( heartRate_CM > BACK_THRESH_CM )
      {
          heartRateBuffer.insert(currIndex % HEART_RATE_EST_MEDIAN_FLT_SIZE, heartRate_FFT);
          IsvalueSelected = 1;
          currIndex++;
      }
      if (heartRate_4Hz_CM > BACK_THRESH_4Hz_CM)
      {
          heartRateBuffer.insert(currIndex % HEART_RATE_EST_MEDIAN_FLT_SIZE, heartRate_FFT_4Hz);
          IsvalueSelected = 1;
          currIndex++;
      }

      if (IsvalueSelected == 0)   // If nothing is selected from the current frame then select the Inter-peak distance Estimate
      {
          heartRateBuffer.insert(currIndex % HEART_RATE_EST_MEDIAN_FLT_SIZE, heartRate_Pk);
          currIndex++;
      }
   }
   else
   {
       heartRateBuffer.insert(localCount % HEART_RATE_EST_MEDIAN_FLT_SIZE, heartRateEstDisplay);
   }

   if (gui_paused != current_gui_status)
   {
   QList<float> heartRateBufferSort = QList<float>::fromVector(heartRateBuffer);
   qSort(heartRateBufferSort.begin(), heartRateBufferSort.end());
   heartRate_OutMedian = heartRateBufferSort.at(HEART_RATE_EST_MEDIAN_FLT_SIZE/2);



if (APPLY_KALMAN_FILTER)
{
   float R;               // Measurement Variance
   float Q;               // State Variance
   float KF_Gain;
   float CM_combined;
   CM_combined = heartRate_CM +  heartRate_4Hz_CM + 10*heartRate_xCorr_CM;
   R = 1/(CM_combined + 0.0001);
   Q = 1e-6F;
   KF_Gain  = Pk/(Pk + R);
   xk = xk + KF_Gain*(heartRate_OutMedian - xk);
   Pk = (1-KF_Gain)*Pk + Q;
   radar_HR_p0 = xk;
}
{
   radar_HR_p0 = heartRate_OutMedian;
}
   // Further Filtering of the Final Heart-rate Estimates

   heartRateOutBufferFinal.insert(localCount % (HEART_RATE_EST_FINAL_OUT_SIZE), radar_HR_p0);
   const auto mean = std::accumulate(heartRateOutBufferFinal.begin(), heartRateOutBufferFinal.end(), .0) / heartRateOutBufferFinal.size();
   double sumMAD;
   double bufferSTD;
   sumMAD = 0;
   for (int indexTemp=0;indexTemp<heartRateOutBufferFinal.size();indexTemp++)
   {
      sumMAD += abs(heartRateOutBufferFinal.at(indexTemp) - mean);
   }
   bufferSTD = sqrt(sumMAD)/heartRateOutBufferFinal.size();

   // Breathing-Rate decision
   float outSumEnergyBreathWfm_thresh;
   float RCS_thresh = THRESH_RCS;
   outSumEnergyBreathWfm_thresh = thresh_breath;
   bool flag_Breathing;

   if ((outSumEnergyBreathWfm < outSumEnergyBreathWfm_thresh) || (maxRCS_updated < RCS_thresh) || (BreathingRate_xCorr_CM <= 0.002))
   {
       flag_Breathing = 0;
       radar_RR_p0 = 0;
       QPalette lcdpaletteNotBreathing = ui->lcdNumber_radar_RR_p0->palette();
       lcdpaletteNotBreathing.setColor(QPalette::Normal, QPalette::Window, Qt::red);
       ui->lcdNumber_radar_RR_p0->setPalette(lcdpaletteNotBreathing);
   }
   else
   {
       flag_Breathing = 1;
       lcdpaletteBreathing.setColor(QPalette::Normal, QPalette::Window, Qt::white);
       ui->lcdNumber_radar_RR_p0->setPalette(lcdpaletteBreathing);


       if (breathRate_CM > THRESH_BREATH_CM )
       {
          radar_RR_p0 = BreathingRate_FFT;
       }
           else
       {
          radar_RR_p0 = BreathingRatePK_Out;
       }
  }


   // Heart-Rate Decision
   float outSumEnergyHeartWfm_thresh;
   outSumEnergyHeartWfm_thresh = thresh_heart;

   if (outSumEnergyHeartWfm < outSumEnergyHeartWfm_thresh || maxRCS_updated < RCS_thresh)
   {
       radar_HR_p0 = 0;
       QPalette lcdpaletteNoHeartRate = ui->lcdNumber_radar_HR_p0->palette();
       lcdpaletteNoHeartRate.setColor(QPalette::Normal, QPalette::Window, Qt::red);
       heartWfm_Out=0;
       ui->lcdNumber_radar_HR_p0->setPalette(lcdpaletteNoHeartRate);
   }
   else
   {
       QPalette lcdpaletteHeartRate = ui->lcdNumber_radar_HR_p0->palette();
       lcdpaletteHeartRate.setColor(QPalette::Normal, QPalette::Window, Qt::white);
       ui->lcdNumber_radar_HR_p0->setPalette(lcdpaletteHeartRate);
   }

   // Palletes for LCD on and off
   QPalette LCD_on = palette();
   LCD_on.setColor(QPalette::Normal, QPalette::Window, Qt::white);

   QPalette LCD_off = palette();
   LCD_off.setColor(QPalette::Normal, QPalette::Window, Qt::lightGray);

   ui->lcdNumber_radar_HR_p0->setPalette(LCD_on);
   ui->lcdNumber_radar_RR_p0->setPalette(LCD_on);
   ui->lcdNumber_integrate_HR_p0->setPalette(LCD_on);
   ui->lcdNumber_integrate_RR_p0->setPalette(LCD_on);

   // Check if p0 to p5 are detected and set LCDs
   // P0
   if (detected_p0 == 1)
   {
       integrate_HR_p0 = radar_HR_p0 + camera_HR_p0;
       integrate_RR_p0 = radar_RR_p0 + camera_RR_p0;
       ui->lcdNumber_camera_HR_p0->setPalette(LCD_on);
       ui->lcdNumber_camera_RR_p0->setPalette(LCD_on);
   }
   else
   {
       camera_HR_p0 = 0;
       camera_RR_p0 = 0;
       integrate_HR_p0 = radar_HR_p0;
       integrate_RR_p0 = radar_RR_p0;
       ui->lcdNumber_camera_HR_p0->setPalette(LCD_off);
       ui->lcdNumber_camera_RR_p0->setPalette(LCD_off);
   }
   // P1
   if (detected_p1 == 1)
   {
       ui->lcdNumber_camera_HR_p1->setPalette(LCD_on);
       ui->lcdNumber_camera_RR_p1->setPalette(LCD_on);
   }
   else
   {
       camera_HR_p1 = 0;
       camera_RR_p1 = 0;
       ui->lcdNumber_camera_HR_p1->setPalette(LCD_off);
       ui->lcdNumber_camera_RR_p1->setPalette(LCD_off);
   }
   //P2
   if (detected_p2 == 1)
   {
       ui->lcdNumber_camera_HR_p2->setPalette(LCD_on);
       ui->lcdNumber_camera_RR_p2->setPalette(LCD_on);
   }
   else
   {
       camera_HR_p2 = 0;
       camera_RR_p2 = 0;
       ui->lcdNumber_camera_HR_p2->setPalette(LCD_off);
       ui->lcdNumber_camera_RR_p2->setPalette(LCD_off);
   }
   //P3
   if (detected_p3 == 1)
   {
       ui->lcdNumber_camera_HR_p3->setPalette(LCD_on);
       ui->lcdNumber_camera_RR_p3->setPalette(LCD_on);
   }
   else
   {
       camera_HR_p3 = 0;
       camera_RR_p3 = 0;
       ui->lcdNumber_camera_HR_p3->setPalette(LCD_off);
       ui->lcdNumber_camera_RR_p3->setPalette(LCD_off);
   }
   //P4
   if (detected_p4 == 1)
   {
       ui->lcdNumber_camera_HR_p4->setPalette(LCD_on);
       ui->lcdNumber_camera_RR_p4->setPalette(LCD_on);
   }
   else
   {
       camera_HR_p4 = 0;
       camera_RR_p4 = 0;
       ui->lcdNumber_camera_HR_p4->setPalette(LCD_off);
       ui->lcdNumber_camera_RR_p4->setPalette(LCD_off);
   }
   //P5
   if (detected_p5 == 1)
   {
       ui->lcdNumber_camera_HR_p5->setPalette(LCD_on);
       ui->lcdNumber_camera_RR_p5->setPalette(LCD_on);
   }
   else
   {
       camera_HR_p5 = 0;
       camera_RR_p5 = 0;
       ui->lcdNumber_camera_HR_p5->setPalette(LCD_off);
       ui->lcdNumber_camera_RR_p5->setPalette(LCD_off);
   }

 if (ui->checkBox_displayPlots->isChecked())
 {
   if (indexTemp ==0)
   {
      for (unsigned int i=0;i<NUM_PTS_DISTANCE_TIME_PLOT;i++)
      {
        xDistTimePlot[i]               = indexTemp;
        yDistTimePlot[i]               = phaseWfm_Out;
        HR_radar_buffer[i]             = radar_HR_p0;
        RR_radar_buffer[indexTemp]     = radar_RR_p0;
        HR_camera_buffer[i]            = camera_HR_p0;
        RR_camera_buffer[indexTemp]    = camera_RR_p0;
        HR_integrate_buffer[i]         = integrate_HR_p0;
        RR_integrate_buffer[indexTemp] = integrate_RR_p0;
       }
   }

   xDistTimePlot[indexTemp]       = indexTemp;
   yDistTimePlot[indexTemp]       = phaseWfm_Out;
   HR_radar_buffer[indexTemp]     = radar_HR_p0;
   RR_radar_buffer[indexTemp]     = radar_RR_p0;
   HR_camera_buffer[indexTemp]    = camera_HR_p0;
   RR_camera_buffer[indexTemp]    = camera_RR_p0;
   HR_integrate_buffer[indexTemp] = integrate_HR_p0;
   RR_integrate_buffer[indexTemp] = integrate_RR_p0;

   ui->RR_plot->graph(0)->setData(xDistTimePlot,RR_radar_buffer);
   ui->RR_plot->graph(1)->setData(xDistTimePlot,RR_camera_buffer);
   ui->RR_plot->graph(2)->setData(xDistTimePlot,RR_integrate_buffer);
   ui->RR_plot->replot();

   ui->HR_plot->graph(0)->setData(xDistTimePlot,HR_radar_buffer);
   ui->HR_plot->graph(1)->setData(xDistTimePlot,HR_camera_buffer);
   ui->HR_plot->graph(2)->setData(xDistTimePlot,HR_integrate_buffer);
   ui->HR_plot->replot();

 }
   // Update Numerical Displays

   // Frame Number
   // ui->lcdNumber_FrameCount->display((int) globalCountOut);

   // Breathing and Heart Rates (Radar)
   QString myString_radar_HR_p0;
   ui->lcdNumber_radar_HR_p0->setDigitCount(3);
   myString_radar_HR_p0.sprintf("%1.0f",radar_HR_p0);
   ui->lcdNumber_radar_HR_p0->display(myString_radar_HR_p0);

   radarPipeline.Write(&radar_HR_p0, sizeof(radar_HR_p0));
   qDebug() << "Wrote HR " << radar_HR_p0;

   QString myString_radar_RR_p0;
   ui->lcdNumber_radar_RR_p0->setDigitCount(8);
   myString_radar_RR_p0.sprintf("%1.0f",radar_RR_p0);
   ui->lcdNumber_radar_RR_p0->display(myString_radar_RR_p0);

   radarPipeline.Write(&radar_RR_p0, sizeof(radar_RR_p0));
   qDebug() << "Wrote RR " << radar_RR_p0;

   QString myString_camera_HR_p0;
   ui->lcdNumber_camera_HR_p0->setDigitCount(3);
   myString_camera_HR_p0.sprintf("%1.0f",camera_HR_p0);
   ui->lcdNumber_camera_HR_p0->display(myString_camera_HR_p0);

   QString myString_camera_RR_p0;
   ui->lcdNumber_camera_RR_p0->setDigitCount(8);
   myString_camera_RR_p0.sprintf("%1.0f",camera_RR_p0);
   ui->lcdNumber_camera_RR_p0->display(myString_camera_RR_p0);

   QString myString_camera_HR_p1;
   ui->lcdNumber_camera_HR_p1->setDigitCount(3);
   myString_camera_HR_p1.sprintf("%1.0f",camera_HR_p1);
   ui->lcdNumber_camera_HR_p1->display(myString_camera_HR_p1);

   QString myString_camera_RR_p1;
   ui->lcdNumber_camera_RR_p1->setDigitCount(8);
   myString_camera_RR_p1.sprintf("%1.0f",camera_RR_p1);
   ui->lcdNumber_camera_RR_p1->display(myString_camera_RR_p1);

   QString myString_camera_HR_p2;
   ui->lcdNumber_camera_HR_p2->setDigitCount(3);
   myString_camera_HR_p2.sprintf("%1.0f",camera_HR_p2);
   ui->lcdNumber_camera_HR_p2->display(myString_camera_HR_p2);

   QString myString_camera_RR_p2;
   ui->lcdNumber_camera_RR_p2->setDigitCount(8);
   myString_camera_RR_p2.sprintf("%1.0f",camera_RR_p2);
   ui->lcdNumber_camera_RR_p2->display(myString_camera_RR_p2);

   QString myString_camera_HR_p3;
   ui->lcdNumber_camera_HR_p3->setDigitCount(3);
   myString_camera_HR_p3.sprintf("%1.0f",camera_HR_p3);
   ui->lcdNumber_camera_HR_p3->display(myString_camera_HR_p3);

   QString myString_camera_RR_p3;
   ui->lcdNumber_camera_RR_p3->setDigitCount(8);
   myString_camera_RR_p3.sprintf("%1.0f",camera_RR_p3);
   ui->lcdNumber_camera_RR_p3->display(myString_camera_RR_p3);

   QString myString_camera_HR_p4;
   ui->lcdNumber_camera_HR_p4->setDigitCount(3);
   myString_camera_HR_p4.sprintf("%1.0f",camera_HR_p4);
   ui->lcdNumber_camera_HR_p4->display(myString_camera_HR_p4);

   QString myString_camera_RR_p4;
   ui->lcdNumber_camera_RR_p4->setDigitCount(8);
   myString_camera_RR_p4.sprintf("%1.0f",camera_RR_p4);
   ui->lcdNumber_camera_RR_p4->display(myString_camera_RR_p4);

   QString myString_camera_HR_p5;
   ui->lcdNumber_camera_HR_p5->setDigitCount(3);
   myString_camera_HR_p5.sprintf("%1.0f",camera_HR_p5);
   ui->lcdNumber_camera_HR_p5->display(myString_camera_HR_p5);

   QString myString_camera_RR_p5;
   ui->lcdNumber_camera_RR_p5->setDigitCount(8);
   myString_camera_RR_p5.sprintf("%1.0f",camera_RR_p5);
   ui->lcdNumber_camera_RR_p5->display(myString_camera_RR_p5);

   QString myString_integrate_HR_p0;
   ui->lcdNumber_integrate_HR_p0->setDigitCount(3);
   myString_integrate_HR_p0.sprintf("%1.0f",integrate_HR_p0);
   ui->lcdNumber_integrate_HR_p0->display(myString_integrate_HR_p0);

   QString myString_integrate_RR_p0;
   ui->lcdNumber_integrate_RR_p0->setDigitCount(8);
   myString_integrate_RR_p0.sprintf("%1.0f",integrate_RR_p0);
   ui->lcdNumber_integrate_RR_p0->display(myString_integrate_RR_p0);

   }
//  qDebug() << "The Process Function took" << timer.elapsed() << "milliseconds";
  } // If GUI-Paused end
 }  //Magic-OK if ends
}

void MainWindow::gui_statusUpdate()
{
    if (current_gui_status == gui_running)
    {
            ui->pushButton_start->setStyleSheet("background-color: red");
            ui->pushButton_stop->setStyleSheet("background-color: none");
            ui->pushButton_pause->setStyleSheet("background-color: none");

            statusBar()->showMessage(tr("Sensor Running"));
    }

    if (current_gui_status == gui_stopped)
    {
            ui->pushButton_stop->setStyleSheet("background-color: red");
            ui->pushButton_start->setStyleSheet("background-color: none");
            ui->pushButton_pause->setStyleSheet("background-color: none");
            statusBar()->showMessage(tr("Sensor Stopped"));
            qDebug()<<"Sensor is Stopped";
            statusBar()->showMessage(tr("Sensor Stopped"));

    }
    if (current_gui_status == gui_paused)
    {
            ui->pushButton_stop->setStyleSheet("background-color: none");
            ui->pushButton_start->setStyleSheet("background-color: none");
            ui->pushButton_pause->setStyleSheet("background-color: red");
            statusBar()->showMessage(tr("GUI is Paused Stopped"));
    }
}

void MainWindow::on_pushButton_pause_clicked()
{
    localCount = 0;
    for (unsigned int i=0;i<NUM_PTS_DISTANCE_TIME_PLOT;i++)
    {
      xDistTimePlot[i]    = 0;
      yDistTimePlot[i]    = 0;
      HR_radar_buffer[i]  = 0;
      RR_radar_buffer[i]  = 0;
      HR_camera_buffer[i] = 0;
      RR_camera_buffer[i] = 0;
     }
    current_gui_status = gui_paused;
    emit gui_statusChanged();
}









